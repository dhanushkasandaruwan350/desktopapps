/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uniquesourcecod;

/**
 *
 * @author dhanushka
 */
public class Modal {
    private String Part_No;
    private String Location;
    private String plant;
    private String Average_Freight;
    private String Forwarder;
    private String Freight_rate_per_unit_Kg_Air;
    private String Freight_rate_per_kg_Ocean;
    private String DHL;
    private String Quantity;
    private String Unit_Wt_in_Kg;
    private String Total_Weight_in_Kg;
    private String Freight_Rate_Air;
    private String Freight_Rate_Ocean;
    private String Freight_Rate_DhL;
    private String Per_Unit_price;
    private String Selling_price;
    private String Unit_Buying_price;
    private String Buying_price;

    public String getPart_No() {
        return Part_No;
    }

    public void setPart_No(String Part_No) {
        this.Part_No = Part_No;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getAverage_Freight() {
        return Average_Freight;
    }

    public void setAverage_Freight(String Average_Freight) {
        this.Average_Freight = Average_Freight;
    }

    public String getForwarder() {
        return Forwarder;
    }

    public void setForwarder(String Forwarder) {
        this.Forwarder = Forwarder;
    }

    public String getFreight_rate_per_unit_Kg_Air() {
        return Freight_rate_per_unit_Kg_Air;
    }

    public void setFreight_rate_per_unit_Kg_Air(String Freight_rate_per_unit_Kg_Air) {
        this.Freight_rate_per_unit_Kg_Air = Freight_rate_per_unit_Kg_Air;
    }

    public String getFreight_rate_per_kg_Ocean() {
        return Freight_rate_per_kg_Ocean;
    }

    public void setFreight_rate_per_kg_Ocean(String Freight_rate_per_kg_Ocean) {
        this.Freight_rate_per_kg_Ocean = Freight_rate_per_kg_Ocean;
    }

    public String getDHL() {
        return DHL;
    }

    public void setDHL(String DHL) {
        this.DHL = DHL;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getUnit_Wt_in_Kg() {
        return Unit_Wt_in_Kg;
    }

    public void setUnit_Wt_in_Kg(String Unit_Wt_in_Kg) {
        this.Unit_Wt_in_Kg = Unit_Wt_in_Kg;
    }

    public String getTotal_Weight_in_Kg() {
        return Total_Weight_in_Kg;
    }

    public void setTotal_Weight_in_Kg(String Total_Weight_in_Kg) {
        this.Total_Weight_in_Kg = Total_Weight_in_Kg;
    }

    public String getFreight_Rate_Air() {
        return Freight_Rate_Air;
    }

    public void setFreight_Rate_Air(String Freight_Rate_Air) {
        this.Freight_Rate_Air = Freight_Rate_Air;
    }

    public String getFreight_Rate_Ocean() {
        return Freight_Rate_Ocean;
    }

    public void setFreight_Rate_Ocean(String Freight_Rate_Ocean) {
        this.Freight_Rate_Ocean = Freight_Rate_Ocean;
    }

    public String getFreight_Rate_DhL() {
        return Freight_Rate_DhL;
    }

    public void setFreight_Rate_DhL(String Freight_Rate_DhL) {
        this.Freight_Rate_DhL = Freight_Rate_DhL;
    }

    public String getPer_Unit_price() {
        return Per_Unit_price;
    }

    public void setPer_Unit_price(String Per_Unit_price) {
        this.Per_Unit_price = Per_Unit_price;
    }

    public String getSelling_price() {
        return Selling_price;
    }

    public void setSelling_price(String Selling_price) {
        this.Selling_price = Selling_price;
    }

    public String getUnit_Buying_price() {
        return Unit_Buying_price;
    }

    public void setUnit_Buying_price(String Unit_Buying_price) {
        this.Unit_Buying_price = Unit_Buying_price;
    }

    public String getBuying_price() {
        return Buying_price;
    }

    public void setBuying_price(String Buying_price) {
        this.Buying_price = Buying_price;
    }
    
    

}
