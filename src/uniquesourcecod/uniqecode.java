package uniquesourcecod;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

public class uniqecode extends javax.swing.JFrame {

    DecimalFormat df = new DecimalFormat("#.####");
    List<Modal> list;
    List<String> keywords = new ArrayList<>();
    Modal m;

    public uniqecode() {
        initComponents();
        this.setLocationRelativeTo(null);
        searching.setVisible(false);
        defaultValues();
        ImageIcon img = new ImageIcon("TE_Logo.png");
        this.setIconImage(img.getImage());
    }

    private List<Modal> findConsumerDetails() {
        List<Modal> list = new ArrayList<>();
        Modal cm = null;
        String fileToParse = "dummy.csv";
        BufferedReader fileReader = null;
        final String DELIMITER = ",";
        try {
            String line = "";
            fileReader = new BufferedReader(new FileReader(fileToParse));

            while ((line = fileReader.readLine()) != null) {
                cm = new Modal();
                String[] tokens = line.split(DELIMITER);
                try {
                    cm.setPart_No(tokens[0].replace("\"", ""));
                    cm.setLocation(tokens[1].replace("\"", ""));
                    cm.setPlant(tokens[2].replace("\"", ""));
                    cm.setAverage_Freight(tokens[3].replace("\"", ""));
                    cm.setForwarder(tokens[4].replace("\"", ""));
                    cm.setFreight_rate_per_unit_Kg_Air(tokens[5].replace("\"", ""));
                    cm.setFreight_rate_per_kg_Ocean(tokens[6].replace("\"", ""));
                    cm.setDHL(tokens[7].replace("\"", ""));
                    cm.setQuantity(tokens[8].replace("\"", ""));
                    cm.setUnit_Wt_in_Kg(tokens[9].replace("\"", ""));
                    cm.setTotal_Weight_in_Kg(tokens[10].replace("\"", ""));
                    cm.setFreight_Rate_Air(tokens[11].replace("\"", ""));
                    cm.setFreight_Rate_Ocean(tokens[12].replace("\"", ""));
                    cm.setFreight_Rate_DhL(tokens[13].replace("\"", ""));
                    cm.setPer_Unit_price(tokens[14].replace("\"", ""));
                    cm.setSelling_price(tokens[15].replace("\"", ""));
                    cm.setUnit_Buying_price(tokens[16].replace("\"", ""));
                    cm.setBuying_price(tokens[17].replace("\"", ""));
                    if (cm.getPart_No().equalsIgnoreCase("Part No.")) {
                        continue;
                    }
                    list.add(cm);
                    keywords.add(tokens[0].replace("\"", ""));
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("check file. it can be empty");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert fileReader != null;
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        searching = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_qty = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel40 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        pup = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        fra = new javax.swing.JLabel();
        fro = new javax.swing.JLabel();
        frd = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel23 = new javax.swing.JLabel();
        sp = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        ubp = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        bp = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        uwkg = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        location = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        plant = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        total_waight = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        average_freight = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        forwarder = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        frpuka = new javax.swing.JLabel();
        frpuko = new javax.swing.JLabel();
        frpukd = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        frpuka1 = new javax.swing.JLabel();
        frpuka2 = new javax.swing.JLabel();
        fpoba = new javax.swing.JLabel();
        fpobo = new javax.swing.JLabel();
        fpobd = new javax.swing.JLabel();
        fposd = new javax.swing.JLabel();
        fposo = new javax.swing.JLabel();
        fposa = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Freight Calculator");
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(254, 254, 254));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 50, 480, 10));

        searching.setFont(new java.awt.Font("Sarai", 1, 14)); // NOI18N
        searching.setForeground(new java.awt.Color(41, 87, 109));
        searching.setText("Searching...");
        jPanel1.add(searching, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 240, -1));

        jLabel15.setFont(new java.awt.Font("Sarai", 1, 36)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(11, 55, 87));
        jLabel15.setText("Freight Calculator");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 10, -1, -1));

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uniquesourcecod/dialog_close(1).png"))); // NOI18N
        jLabel17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 10, 30, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1160, -1));

        jPanel2.setBackground(new java.awt.Color(254, 254, 254));

        jPanel3.setBackground(new java.awt.Color(254, 254, 254));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, " Search by part no:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Sarai", 1, 18), new java.awt.Color(171, 162, 162))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Sarai", 1, 15)); // NOI18N
        jLabel2.setText("PART NO : ");

        txt_qty.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_qtyKeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Sarai", 1, 15)); // NOI18N
        jLabel3.setText("QUANTITY : ");

        jLabel6.setBackground(new java.awt.Color(46, 119, 141));
        jLabel6.setFont(new java.awt.Font("Sarai", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(254, 254, 254));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uniquesourcecod/summer_font.png"))); // NOI18N
        jLabel6.setText("Import Data");
        jLabel6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jLabel6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel6.setOpaque(true);
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });

        jComboBox1.setEditable(true);

        jLabel40.setBackground(new java.awt.Color(57, 137, 137));
        jLabel40.setFont(new java.awt.Font("Sarai", 1, 24)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(246, 246, 246));
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uniquesourcecod/edit_find(1).png"))); // NOI18N
        jLabel40.setText("Search");
        jLabel40.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jLabel40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel40.setOpaque(true);
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel40MouseClicked(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Sarai", 1, 15)); // NOI18N
        jLabel16.setText("Unit Selling Price :");

        pup.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pupKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_qty, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pup, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jComboBox1, txt_qty});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel2))
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txt_qty, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel16)
                                .addComponent(pup, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel40)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jComboBox1, txt_qty});

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel40, jLabel6, pup});

        jPanel4.setBackground(new java.awt.Color(254, 254, 254));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Search Results", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Sarai", 1, 18), new java.awt.Color(171, 162, 162))); // NOI18N

        jLabel11.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel11.setText("UNIT WT IN KG : ");

        jLabel29.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(59, 148, 166));
        jLabel29.setText("AIR - 100%");

        jLabel12.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel12.setText("TOTAL WEIGHT IN KG : ");

        jLabel30.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(59, 148, 166));
        jLabel30.setText("AIR - 100%");

        jLabel32.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel32.setText("UNIT BUYING PRICE : ");

        jLabel33.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(59, 148, 166));
        jLabel33.setText("AIR - 100%");

        jLabel34.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel34.setText("BUYING PRICE : ");

        jLabel35.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(59, 148, 166));
        jLabel35.setText("AIR - 100%");

        jPanel5.setBackground(new java.awt.Color(211, 81, 81));
        jPanel5.setOpaque(false);

        jLabel14.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel14.setText("FREIGHT RATE : ");

        fra.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fra.setForeground(new java.awt.Color(59, 148, 166));
        fra.setText("AIR - 100%");

        fro.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fro.setForeground(new java.awt.Color(59, 148, 166));
        fro.setText("OCEAN - 100%");

        frd.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        frd.setForeground(new java.awt.Color(59, 148, 166));
        frd.setText("DHL - 100%");

        jLabel23.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel23.setText("TOTAL SELLING PRICE : ");

        sp.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        sp.setForeground(new java.awt.Color(59, 148, 166));

        jLabel36.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel36.setText("PER UNIT BUYING PRICE : ");

        ubp.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        ubp.setForeground(new java.awt.Color(59, 148, 166));

        jLabel38.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel38.setText("TOTAL BUYING PRICE : ");

        bp.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        bp.setForeground(new java.awt.Color(59, 148, 166));

        jLabel41.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel41.setText("UNIT WAIGHT (KG) : ");

        uwkg.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        uwkg.setForeground(new java.awt.Color(59, 148, 166));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel41, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel38, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel36, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel23, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ubp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(uwkg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fra, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(frd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fro, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {bp, fra, frd, fro, sp, ubp, uwkg});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fra))
                .addGap(4, 4, 4)
                .addComponent(fro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(frd, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sp, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel36, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ubp, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel38, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bp, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel41, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(uwkg, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(67, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {bp, fra, fro, sp, ubp, uwkg});

        jPanel6.setOpaque(false);

        jLabel4.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel4.setText("LOCATION : ");

        location.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        location.setForeground(new java.awt.Color(59, 148, 166));

        jLabel5.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel5.setText("PLANT : ");

        plant.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        plant.setForeground(new java.awt.Color(59, 148, 166));

        jLabel7.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel7.setText("TOTAL WAIGHT (KG) :");

        total_waight.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        total_waight.setForeground(new java.awt.Color(59, 148, 166));

        jLabel8.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel8.setText("AVERAGE FREIGHT : ");

        average_freight.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        average_freight.setForeground(new java.awt.Color(59, 148, 166));

        jLabel9.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel9.setText("FORWARDER : ");

        forwarder.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        forwarder.setForeground(new java.awt.Color(59, 148, 166));

        jLabel10.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        jLabel10.setText(" FREIGHT RATE : ");

        frpuka.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        frpuka.setForeground(new java.awt.Color(59, 148, 166));
        frpuka.setText("AIR - 100%");

        frpuko.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        frpuko.setForeground(new java.awt.Color(59, 148, 166));
        frpuko.setText("OCEAN - 100%");

        frpukd.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        frpukd.setForeground(new java.awt.Color(59, 148, 166));
        frpukd.setText("DHL - 100%");

        jLabel13.setFont(new java.awt.Font("Serif", 1, 10)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("PER UNIT KG");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(91, 91, 91)
                        .addComponent(forwarder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(38, 38, 38)
                        .addComponent(average_freight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(total_waight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(86, 86, 86)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(plant, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(location, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(jLabel13))
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(65, 65, 65)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(frpukd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(frpuka, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(frpuko, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(location, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel5))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(plant, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(22, 22, 22)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(total_waight, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(22, 22, 22)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(average_freight, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(forwarder, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(frpuka)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(frpuko, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(frpukd)
                .addContainerGap())
        );

        frpuka1.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        frpuka1.setForeground(new java.awt.Color(52, 97, 106));
        frpuka1.setText("Freight percentage on Buying ");

        frpuka2.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        frpuka2.setForeground(new java.awt.Color(52, 97, 106));
        frpuka2.setText("Freight percentage on Selling");

        fpoba.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fpoba.setForeground(new java.awt.Color(59, 148, 166));
        fpoba.setText("AIR - 100%");

        fpobo.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fpobo.setForeground(new java.awt.Color(59, 148, 166));
        fpobo.setText("AIR - 100%");

        fpobd.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fpobd.setForeground(new java.awt.Color(59, 148, 166));
        fpobd.setText("AIR - 100%");

        fposd.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fposd.setForeground(new java.awt.Color(59, 148, 166));
        fposd.setText("AIR - 100%");

        fposo.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fposo.setForeground(new java.awt.Color(59, 148, 166));
        fposo.setText("AIR - 100%");

        fposa.setFont(new java.awt.Font("Serif", 1, 15)); // NOI18N
        fposa.setForeground(new java.awt.Color(59, 148, 166));
        fposa.setText("AIR - 100%");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/uniquesourcecod/TE_Logo.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(75, 75, 75)
                                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(82, 82, 82)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel34)
                                .addGap(77, 77, 77)
                                .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel32)
                                .addGap(77, 77, 77)
                                .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                                .addGap(17, 17, 17)))
                        .addGap(44, 44, 44))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(fpoba, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                            .addComponent(fpobo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fpobd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23))))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(frpuka1)
                        .addGap(126, 126, 126)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(frpuka2)
                            .addComponent(fposa, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                            .addComponent(fposo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(fposd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(58, 58, 58)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(32, 32, 32))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(frpuka1)
                            .addComponent(frpuka2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(fpoba)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fpobo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fpobd))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(fposa)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fposo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fposd)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 494, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 1160, 640));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        list = findConsumerDetails();
        for (Modal m : list) {
            jComboBox1.addItem(m.getPart_No());
            System.out.println(m.getPart_No());
        }
        JOptionPane.showMessageDialog(null, list.size() + " records found!", "Data import finished!", JOptionPane.INFORMATION_MESSAGE);
        jComboBox1.setSelectedItem("");
        AutoCompleteDecorator.decorate(jComboBox1);
    }//GEN-LAST:event_jLabel6MouseClicked

    private void jLabel40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseClicked
        searching.setVisible(true);
        m = null;
        if (list != null) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(uniqecode.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Modal m : list) {
                if (m.getPart_No().equalsIgnoreCase(jComboBox1.getSelectedItem().toString())) {
                    this.m = m;
                    break;
                }
            }
            calculations();
        } else {
            JOptionPane.showMessageDialog(rootPane, "Please import a valid datasheet and continue.", "No Data Found!", JOptionPane.ERROR_MESSAGE);
        }
        searching.setVisible(false);
    }//GEN-LAST:event_jLabel40MouseClicked

    private void txt_qtyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_qtyKeyReleased
        if (txt_qty.getText() == null || txt_qty.getText().length() < 1) {
            return;
        }
        m.setQuantity(txt_qty.getText());
        calculations();
    }//GEN-LAST:event_txt_qtyKeyReleased

    private void pupKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pupKeyReleased
        if (pup.getText().length() > 0) {
            m.setPer_Unit_price(pup.getText());
            calculations();
        }
    }//GEN-LAST:event_pupKeyReleased

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
        this.dispose();
    }//GEN-LAST:event_jLabel17MouseClicked

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(uniqecode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(uniqecode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(uniqecode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(uniqecode.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new uniqecode().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel average_freight;
    private javax.swing.JLabel bp;
    private javax.swing.JLabel forwarder;
    private javax.swing.JLabel fpoba;
    private javax.swing.JLabel fpobd;
    private javax.swing.JLabel fpobo;
    private javax.swing.JLabel fposa;
    private javax.swing.JLabel fposd;
    private javax.swing.JLabel fposo;
    private javax.swing.JLabel fra;
    private javax.swing.JLabel frd;
    private javax.swing.JLabel fro;
    private javax.swing.JLabel frpuka;
    private javax.swing.JLabel frpuka1;
    private javax.swing.JLabel frpuka2;
    private javax.swing.JLabel frpukd;
    private javax.swing.JLabel frpuko;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JLabel location;
    private javax.swing.JLabel plant;
    private javax.swing.JTextField pup;
    private javax.swing.JLabel searching;
    private javax.swing.JLabel sp;
    private javax.swing.JLabel total_waight;
    private javax.swing.JTextField txt_qty;
    private javax.swing.JLabel ubp;
    private javax.swing.JLabel uwkg;
    // End of variables declaration//GEN-END:variables

    private void setValues() {
        if (m == null) {
            JOptionPane.showMessageDialog(rootPane, "No records found!", "Empty result", JOptionPane.INFORMATION_MESSAGE);
        }
        txt_qty.setText(m.getQuantity());
        location.setText(m.getLocation());
        plant.setText(m.getPlant());
        total_waight.setText(m.getTotal_Weight_in_Kg());
        average_freight.setText(m.getAverage_Freight());
        forwarder.setText(m.getForwarder());
        frpuka.setText("A- " + m.getFreight_rate_per_unit_Kg_Air());
        frpuko.setText("O- " + m.getFreight_rate_per_kg_Ocean());
        frpukd.setText("D- " + m.getDHL());
        uwkg.setText(m.getUnit_Wt_in_Kg());
        fra.setText("A- " + m.getFreight_Rate_Air());
        fro.setText("O- " + m.getFreight_Rate_Ocean());
        frd.setText("D- " + m.getFreight_Rate_DhL());
        pup.setText(m.getPer_Unit_price());
        sp.setText(m.getSelling_price());
        ubp.setText(m.getUnit_Buying_price());
        bp.setText(m.getBuying_price());
    }

    private void defaultValues() {
        txt_qty.setText("-");
        location.setText("-");
        plant.setText("-");
        total_waight.setText("-");
        average_freight.setText("-");
        forwarder.setText("-");
        frpuka.setText("-");
        frpuko.setText("-");
        frpukd.setText("-");
        uwkg.setText("-");
        fra.setText("-");
        fro.setText("-");
        frd.setText("-");
        pup.setText("-");
        sp.setText("-");
        ubp.setText("-");
        bp.setText("-");
        fpoba.setText("-");
        fpobo.setText("-");
        fpobd.setText("-");
        fposa.setText("-");
        fposo.setText("-");
        fposd.setText("-");
    }

    private void calculations() {
        m.setTotal_Weight_in_Kg(df.format(Double.parseDouble(m.getUnit_Wt_in_Kg()) * Double.parseDouble(m.getQuantity())) + "");
        m.setFreight_Rate_Air(df.format(Double.parseDouble(m.getFreight_rate_per_unit_Kg_Air()) * Double.parseDouble(m.getTotal_Weight_in_Kg())) + "");
        m.setFreight_Rate_Ocean(df.format(Double.parseDouble(m.getFreight_rate_per_kg_Ocean()) * Double.parseDouble(m.getTotal_Weight_in_Kg())) + "");
        m.setFreight_Rate_DhL(df.format(Double.parseDouble(m.getDHL()) * Double.parseDouble(m.getTotal_Weight_in_Kg())) + "");
        m.setSelling_price(df.format(Double.parseDouble(m.getQuantity()) * Double.parseDouble(m.getPer_Unit_price())) + "");
        m.setBuying_price(df.format(Double.parseDouble(m.getQuantity()) * Double.parseDouble(m.getUnit_Buying_price())) + "");
        percentages();
        setValues();
    }

    private void percentages() {
        fpoba.setText("A-" + df.format((Double.parseDouble(m.getFreight_Rate_Air()) * 100) / Double.parseDouble(m.getBuying_price())) + "");
        fpobo.setText("O-" + df.format((Double.parseDouble(m.getFreight_Rate_Ocean()) * 100) / Double.parseDouble(m.getBuying_price())) + "");
        fpobd.setText("D-" + df.format((Double.parseDouble(m.getFreight_Rate_DhL()) * 100) / Double.parseDouble(m.getBuying_price())) + "");

        fposa.setText("A-" + df.format((Double.parseDouble(m.getFreight_Rate_Air()) * 100) / Double.parseDouble(m.getSelling_price())) + "");
        fposo.setText("O-" + df.format((Double.parseDouble(m.getFreight_Rate_Ocean()) * 100) / Double.parseDouble(m.getSelling_price())) + "");
        fposd.setText("D-" + df.format((Double.parseDouble(m.getFreight_Rate_DhL()) * 100) / Double.parseDouble(m.getSelling_price())) + "");
    }
}
