/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardmarket;

/**
 *
 * @author dhanushka
 */
public class Dto {

    private String rarity;
    private String number;
    private String price;
    private String image;
    private String expension;
    private String name;
    private String name2;
    private String reprints;
    private String avarage;
    

    
    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getExpension() {
        return expension;
    }

    public void setExpension(String expension) {
        this.expension = expension;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReprints() {
        return reprints;
    }

    public void setReprints(String reprints) {
        this.reprints = reprints;
    }

    public String getAvarage() {
        return avarage;
    }

    public void setAvarage(String avarage) {
        this.avarage = avarage;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
    
    
}
