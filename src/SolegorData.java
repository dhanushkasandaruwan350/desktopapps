/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dhanushka
 */
public class SolegorData {

    private int id;
    private String name;
    private String address;
    private String web;
    private String number;
    private String bienenvente;
    private String bienenlocationtot;
    private String appatrment;
    private String parking;
    private String bureaux;
    private String zoned = "";
    private String zoned2 = "";
    private String zoned3 = "";
    private String zoned4 = "";
    private String zoned5 = "";
    private String zoned6 = "";
    private String team = "";
    private String team2 = "";
    private String team3 = "";
    private String team4 = "";
    private String team5 = "";
    private String team6= "";
    private String team7 = "";
    private String team8 = "";

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBienenvente() {
        return bienenvente;
    }

    public void setBienenvente(String bienenvente) {
        this.bienenvente = bienenvente;
    }

    public String getBienenlocationtot() {
        return bienenlocationtot;
    }

    public void setBienenlocationtot(String bienenlocationtot) {
        this.bienenlocationtot = bienenlocationtot;
    }

    public String getAppatrment() {
        return appatrment;
    }

    public void setAppatrment(String appatrment) {
        this.appatrment = appatrment;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getBureaux() {
        return bureaux;
    }

    public void setBureaux(String bureaux) {
        this.bureaux = bureaux;
    }

    public String getZoned() {
        return zoned.trim();
    }

    public void setZoned(String zoned) {
        this.zoned = zoned;
    }

    public String getTeam() {
        return team.trim();
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getZoned2() {
        return zoned2.trim();
    }

    public void setZoned2(String zoned2) {
        this.zoned2 = zoned2;
    }

    public String getZoned3() {
        return zoned3.trim();
    }

    public void setZoned3(String zoned3) {
        this.zoned3 = zoned3;
    }

    public String getZoned4() {
        return zoned4.trim();
    }

    public void setZoned4(String zoned4) {
        this.zoned4 = zoned4;
    }

    public String getZoned5() {
        return zoned5.trim();
    }

    public void setZoned5(String zoned5) {
        this.zoned5 = zoned5;
    }

    public String getZoned6() {
        return zoned6.trim();
    }

    public void setZoned6(String zoned6) {
        this.zoned6 = zoned6;
    }

    public String getTeam2() {
        return team2.trim();
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getTeam3() {
        return team3.trim();
    }

    public void setTeam3(String team3) {
        this.team3 = team3;
    }

    public String getTeam4() {
        return team4.trim();
    }

    public void setTeam4(String team4) {
        this.team4 = team4;
    }

    public String getTeam5() {
        return team5.trim();
    }

    public void setTeam5(String team5) {
        this.team5 = team5;
    }

    public String getTeam6() {
        return team6.trim();
    }

    public void setTeam6(String team6) {
        this.team6 = team6;
    }

    public String getTeam7() {
        return team7.trim();
    }

    public void setTeam7(String team7) {
        this.team7 = team7;
    }

    public String getTeam8() {
        return team8.trim();
    }

    public void setTeam8(String team8) {
        this.team8 = team8;
    }

}
