/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bitskins;

import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import util.DriverInitializer;
import util.Trial;

public class bitskins extends javax.swing.JFrame {

    private FirefoxDriver driver = null;
    CommonBitskins bitskins = null;
    List<CommonBitskins> bitskinsList = new ArrayList<>();
    boolean running = false;
    boolean expier = false;

    public bitskins() {
        initComponents();
        this.setLocationRelativeTo(null);
//        if (new Trial().getTrialDay() > 16) {
//            JOptionPane.showMessageDialog(rootPane, "Your trial is over!", "WARNING!", JOptionPane.WARNING_MESSAGE);
//            expier = true;
//        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        nameText = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        pages = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(30, 47, 65));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bitskins/k.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 480));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText(" INPUT NAME");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(413, 180, 220, 30));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("BITSKINS MENU");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 90, 360, 30));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 240, 270, 10));

        nameText.setBackground(new java.awt.Color(35, 47, 65));
        nameText.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        nameText.setForeground(new java.awt.Color(255, 255, 255));
        nameText.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nameText.setText("Berlin 2019");
        nameText.setBorder(null);
        nameText.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        nameText.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nameTextMouseClicked(evt);
            }
        });
        nameText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTextActionPerformed(evt);
            }
        });
        jPanel1.add(nameText, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 210, 270, 30));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 130, 260, 10));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/bitskins/Unt.png"))); // NOI18N
        jLabel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 340, 100, 40));

        jLabel5.setFont(new java.awt.Font("Serif", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(20, 137, 189));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(29, 115, 131), 2));
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 410, 357, 50));

        pages.setBackground(new java.awt.Color(35, 47, 65));
        pages.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        pages.setForeground(new java.awt.Color(255, 255, 255));
        pages.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        pages.setText("20");
        pages.setBorder(null);
        pages.setDisabledTextColor(new java.awt.Color(204, 204, 204));
        pages.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pagesMouseClicked(evt);
            }
        });
        pages.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pagesActionPerformed(evt);
            }
        });
        jPanel1.add(pages, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 280, 270, 30));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Page Count");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 250, 210, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 737, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nameTextMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nameTextMouseClicked

        nameText.setText("");
    }//GEN-LAST:event_nameTextMouseClicked

    private void nameTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTextActionPerformed

    }//GEN-LAST:event_nameTextActionPerformed

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        if (expier) {
            JOptionPane.showMessageDialog(rootPane, "Your trial is over!", "WARNING!", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (!running) {
            if (nameText.getText().length() < 2) {
                JOptionPane.showMessageDialog(rootPane, "Please enter valid name before search.", "Error Message !", JOptionPane.WARNING_MESSAGE);
                return;
            } else {
                new Thread(() -> {
                    try {
                        running = true;
                        initialise();
                    } catch (Exception ex) {
                        Logger.getLogger(bitskins.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }).start();
            }
        } else {
            jLabel5.setText("Already running...");
            JOptionPane.showMessageDialog(rootPane, "Please wait until current scrape finish.", "System Busy !", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jLabel4MouseClicked

    private void pagesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pagesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_pagesMouseClicked

    private void pagesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pagesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pagesActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(bitskins.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(bitskins.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(bitskins.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(bitskins.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new bitskins().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField nameText;
    private javax.swing.JTextField pages;
    // End of variables declaration//GEN-END:variables

    private void initialise() {
        jLabel5.setText("initializing drivers..");
        driver = new DriverInitializer().getFirefoxDriver();
        scrape();
    }

    private void scrape() {
        int page = 1;
        WebElement webElement;
        String wear = "";
        String link = "";
        WebElement leax;
        String s;
        WebElement elementByXPath;
        WebElement row;
        String lastPage = "page=";
        int pages = Integer.parseInt(this.pages.getText());
        int showPages = pages;
        while (true) {
            driver.get("https://bitskins.com/?appid=730&page=" + page + "&is_stattrak=0&has_stickers=1&is_souvenir=-1&show_trade_delayed_items=0&sort_by=price&order=asc");
            elementByXPath = driver.findElementByXPath("/html/body/div[4]/div/div[2]/div");
            row = elementByXPath.findElement(By.className("row"));
            for (WebElement element : row.findElements(By.xpath("./*"))) {

                for (WebElement x : element.findElement(By.className("panel-default")).findElements(By.tagName("img"))) {

                    if (x.getAttribute("data-original-title").contains(nameText.getText())) {
                        if (x.getAttribute("data-original-title").contains("Wear: 0%")) {
                            bitskins = new CommonBitskins();
                            bitskins.setLink(element.findElement(By.className("panel-default")).findElement(By.className("item-icon")).findElements(By.tagName("a")).get(4).getAttribute("href"));
                            bitskinsList.add(bitskins);
                            System.out.println(x.getAttribute("data-original-title"));
                            jLabel5.setText(x.getAttribute("data-original-title"));
                            System.out.println(element.findElement(By.className("panel-default")).findElement(By.className("item-icon")).findElements(By.tagName("a")).get(4).getAttribute("href"));
                        }
                    }
                    x = null;
                    System.gc();
                }

                element = null;
                System.gc();
            }
            if (page >= pages) {
                break;
            }
            page++;
            showPages = showPages - 1;
            this.pages.setText(showPages + "Pages to go");
            lastPage = lastPage + "" + page;
            System.out.println("<====== Next Page :" + page + "=======>");
            jLabel5.setText(bitskinsList.size() + " - Items Collected");
        }

        try {
            this.pages.setText("0");
            jLabel5.setText("writing...");
            createExcelFile(bitskinsList);
            jLabel5.setText("Done!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createExcelFile(List<CommonBitskins> bitskinsList) throws IOException {
        System.out.println("writing");
        CSVWriter csvWriter = new CSVWriter(new FileWriter("./bitskins.csv"));
        List<String[]> csvRows = new LinkedList<String[]>();
        csvRows.add(new String[]{
            "",
            "LINK"
        });

        int count = 1;
        for (CommonBitskins data : bitskinsList) {
            csvRows.add(new String[]{
                count + "",
                data.getLink()
            });
            count++;
        }
        csvWriter.writeAll(csvRows);
        csvWriter.close();
    }
}
