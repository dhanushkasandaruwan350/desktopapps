/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oriant.googlereviewbot;

/**
 *
 * @author Chandimal
 */
public class GoogleMapCommon {

    private int googleID;
    private String email;
    private String password;
    private String conformEmail;
    private String country;
    private String city;
    private String level;
    
    

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the googleID
     */
    public int getGoogleID() {
        return googleID;
    }

    /**
     * @param googleID the googleID to set
     */
    public void setGoogleID(int googleID) {
        this.googleID = googleID;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the conformEmail
     */
    public String getConformEmail() {
        return conformEmail;
    }

    /**
     * @param conformEmail the conformEmail to set
     */
    public void setConformEmail(String conformEmail) {
        this.conformEmail = conformEmail;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

}
