/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oriant.googlereviewbot;

import com.opencsv.CSVWriter;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import util.DriverInitializer;

public class GoogleReviewBotView extends javax.swing.JFrame {

    FirefoxDriver driver = null;
    GoogleMapCommon googleMapCommon = null;
    List<GoogleMapCommon> googleMapCommonList = new ArrayList<>();
    int count = 0;
    String message = "";
    List<String> places = new ArrayList<>();

    public GoogleReviewBotView() {
        initComponents();
        LblShowRedCloseBtn.setVisible(false);
        LblShowGreenCloseBtn.setVisible(false);
    }

    private void getReview() {
        Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable t = c.getContents(this);
        if (t == null) {
            message = "really good place";
        }
        try {
            message = (String) t.getTransferData(DataFlavor.stringFlavor);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "We got some trouble with gettting clipboard text \n Please contact developers.", "System clipboard not responding", JOptionPane.ERROR_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LblShowRedCloseBtn1 = new javax.swing.JLabel();
        LblShowGreenCloseBtn1 = new javax.swing.JLabel();
        LblShowRedCloseBtn = new javax.swing.JLabel();
        LblShowGreenCloseBtn = new javax.swing.JLabel();
        Lblheadre = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        LblPostedReview = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        country = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        LblCurrentAccount = new javax.swing.JLabel();
        LblCurrent = new javax.swing.JLabel();
        LblLastAccount = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        LblLevelUp1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        stat = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(238, 238, 238));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblShowRedCloseBtn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblShowRedCloseBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/11111111111111.png"))); // NOI18N
        LblShowRedCloseBtn1.setToolTipText("Exit");
        LblShowRedCloseBtn1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblShowRedCloseBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblShowRedCloseBtn1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblShowRedCloseBtn1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblShowRedCloseBtn1MouseExited(evt);
            }
        });
        jPanel1.add(LblShowRedCloseBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 3, 15, 15));

        LblShowGreenCloseBtn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblShowGreenCloseBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/222222.png"))); // NOI18N
        LblShowGreenCloseBtn1.setToolTipText("Minimize");
        LblShowGreenCloseBtn1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblShowGreenCloseBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblShowGreenCloseBtn1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblShowGreenCloseBtn1MouseExited(evt);
            }
        });
        jPanel1.add(LblShowGreenCloseBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 3, 15, 15));

        LblShowRedCloseBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblShowRedCloseBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/MouseRed.png"))); // NOI18N
        LblShowRedCloseBtn.setToolTipText("Exit");
        LblShowRedCloseBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblShowRedCloseBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblShowRedCloseBtnMouseClicked(evt);
            }
        });
        jPanel1.add(LblShowRedCloseBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 3, 15, 15));

        LblShowGreenCloseBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblShowGreenCloseBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/MouseGreen.png"))); // NOI18N
        LblShowGreenCloseBtn.setToolTipText("Minimize");
        LblShowGreenCloseBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblShowGreenCloseBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblShowGreenCloseBtnMouseClicked(evt);
            }
        });
        jPanel1.add(LblShowGreenCloseBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(25, 3, 15, 15));

        Lblheadre.setBackground(new java.awt.Color(153, 153, 153));
        Lblheadre.setOpaque(true);
        jPanel1.add(Lblheadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 980, 20));

        jLabel13.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jLabel13.setText("Posted Review Count :");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 230, 170, -1));

        LblPostedReview.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jPanel1.add(LblPostedReview, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 230, 380, 20));

        jLabel4.setFont(new java.awt.Font("Lucida Bright", 1, 13)); // NOI18N
        jLabel4.setText("Business Type :");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(42, 180, 110, -1));

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/start111.png"))); // NOI18N
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 250, 28, -1));

        jButton1.setBackground(new java.awt.Color(0, 102, 51));
        jButton1.setFont(new java.awt.Font("Lucida Bright", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("  Start");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 250, 160, 35));
        jPanel1.add(country, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 140, 260, 30));
        jPanel1.add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, 260, 30));

        jLabel6.setFont(new java.awt.Font("Lucida Bright", 1, 13)); // NOI18N
        jLabel6.setText("Select Country :");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 110, 130, -1));

        LblCurrentAccount.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jPanel1.add(LblCurrentAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 205, 200, -1));

        LblCurrent.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jPanel1.add(LblCurrent, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 190, 420, 20));

        LblLastAccount.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        LblLastAccount.setText("testname22183@gmail.com");
        jPanel1.add(LblLastAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 110, 440, -1));

        jLabel10.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jLabel10.setText("Last Account :");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 110, 100, -1));

        jLabel11.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jLabel11.setText("Level Up To :");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 150, 100, -1));

        jLabel12.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jLabel12.setText("Current Account :");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 190, 130, -1));

        jLabel14.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 310, 210));
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 310, 30, 30));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/LvlUp.png"))); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(368, 142, 25, 25));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/Current.png"))); // NOI18N
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(368, 182, 25, 25));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/Last.png"))); // NOI18N
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(368, 97, 25, 25));

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/Review.png"))); // NOI18N
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(368, 222, 25, 25));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/googlereviewbot/logo22.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 330, 50, 50));

        jLabel2.setFont(new java.awt.Font("Lucida Bright", 3, 18)); // NOI18N
        jLabel2.setText("Google Review Bot Application");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 50, 290, 30));

        jLabel17.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel17.setText("@ Native Solutions");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 360, -1, -1));

        LblLevelUp1.setFont(new java.awt.Font("Lucida Bright", 1, 14)); // NOI18N
        jPanel1.add(LblLevelUp1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 150, 450, 20));

        jLabel5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 90, 610, 210));
        jPanel1.add(stat, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 356, 780, 20));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 970, 387));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void LblShowRedCloseBtn1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblShowRedCloseBtn1MouseEntered
        LblShowRedCloseBtn.setVisible(true);
        LblShowRedCloseBtn1.setVisible(false);
    }//GEN-LAST:event_LblShowRedCloseBtn1MouseEntered

    private void LblShowRedCloseBtn1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblShowRedCloseBtn1MouseExited
        LblShowRedCloseBtn.setVisible(false);
        LblShowRedCloseBtn1.setVisible(true);
    }//GEN-LAST:event_LblShowRedCloseBtn1MouseExited

    private void LblShowGreenCloseBtn1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblShowGreenCloseBtn1MouseEntered
        LblShowGreenCloseBtn.setVisible(true);
        LblShowGreenCloseBtn1.setVisible(false);
    }//GEN-LAST:event_LblShowGreenCloseBtn1MouseEntered

    private void LblShowGreenCloseBtn1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblShowGreenCloseBtn1MouseExited
        LblShowGreenCloseBtn.setVisible(false);
        LblShowGreenCloseBtn1.setVisible(true);
    }//GEN-LAST:event_LblShowGreenCloseBtn1MouseExited

    private void LblShowRedCloseBtn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblShowRedCloseBtn1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_LblShowRedCloseBtn1MouseClicked

    private void LblShowRedCloseBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblShowRedCloseBtnMouseClicked
        System.exit(0);        // TODO add your handling code here:
    }//GEN-LAST:event_LblShowRedCloseBtnMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new Thread(() -> {
            try {
                jButton1.setText("Please Wait");
                initialise();
            } catch (InterruptedException ex) {
                Logger.getLogger(GoogleReviewBotView.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(GoogleReviewBotView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void LblShowGreenCloseBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblShowGreenCloseBtnMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_LblShowGreenCloseBtnMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GoogleReviewBotView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GoogleReviewBotView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GoogleReviewBotView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GoogleReviewBotView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GoogleReviewBotView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblCurrent;
    private javax.swing.JLabel LblCurrentAccount;
    private javax.swing.JLabel LblLastAccount;
    private javax.swing.JLabel LblLevelUp1;
    private javax.swing.JLabel LblPostedReview;
    private javax.swing.JLabel LblShowGreenCloseBtn;
    private javax.swing.JLabel LblShowGreenCloseBtn1;
    private javax.swing.JLabel LblShowRedCloseBtn;
    private javax.swing.JLabel LblShowRedCloseBtn1;
    private javax.swing.JLabel Lblheadre;
    private javax.swing.JTextField country;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JLabel stat;
    // End of variables declaration//GEN-END:variables

    public void initialise() throws Exception {
        for (GoogleMapCommon mapCommonEmail : findAccounts()) {
            driver = new DriverInitializer().getFirefoxDriver();
            scrape(mapCommonEmail);
            if (places.size() < 20) {
                scrapeGoogleMap();
            }
            postReviews();
            LblLastAccount.setText(mapCommonEmail.getEmail());
            checkLevel(mapCommonEmail);
            System.out.println("DDD");
            driver.close();
        }
        jButton1.setText("Start");

    }

    private void scrape(GoogleMapCommon mapCommonEmail) throws InterruptedException {
        stat.setText("Login to " + mapCommonEmail.getEmail());
        LblCurrent.setText(mapCommonEmail.getEmail());
        driver.get("https://accounts.google.com/ServiceLogin/identifier?elo=1&flowName=GlifWebSignIn&flowEntry=AddSession");
        WebElement userName = driver.findElementByXPath("//*[@id=\"identifierId\"]");
        userName.sendKeys(mapCommonEmail.getEmail());
        userName.sendKeys(Keys.ENTER);
        Thread.sleep(3000);
        WebElement passwordInput = driver.findElementByXPath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div/div/div/div/div[1]/div/div[1]/input");
        passwordInput.sendKeys(mapCommonEmail.getPassword());
        passwordInput.sendKeys(Keys.ENTER);
        Thread.sleep(5000);
        try {
            WebElement recoveryEmail = driver.findElementByXPath("/html/body/div[1]/div[1]/div[2]/div[2]/div/div/div[2]/div/div/div/form/span/section/div/div/div/ul/li[1]");
            recoveryEmail.click();
            Thread.sleep(5000);
            WebElement recoveryEmailInput = driver.findElementByCssSelector("#identifierId");
            recoveryEmailInput.sendKeys(mapCommonEmail.getConformEmail());
            recoveryEmailInput.sendKeys(Keys.ENTER);
            Thread.sleep(2000);
            driver.get("https://www.google.com/maps/search/");
            Thread.sleep(5000);
            driver.findElementByCssSelector("#searchboxinput").sendKeys(jTextField2.getText() + " " + country.getText());
            driver.findElementByCssSelector("#searchboxinput").sendKeys(Keys.ENTER);
        } catch (NoSuchElementException e) {
//            e.printStackTrace();
            driver.get("https://www.google.com/maps/search/");
            Thread.sleep(5000);
            driver.findElementByCssSelector("#searchboxinput").sendKeys(jTextField2.getText() + " " + country.getText());
            driver.findElementByCssSelector("#searchboxinput").sendKeys(Keys.ENTER);
        }
    }

    private void scrapeGoogleMap() throws InterruptedException {
        places = new ArrayList<>();
        String resultPage = driver.getCurrentUrl();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        Thread.sleep(10000);
        boolean goBack = false;

        WebElement elementByXPath1 = null;
        WebElement nextBtn = null;
        WebElement elementByXPath = null;
        while (true) {
            goBack = false;

            elementByXPath = driver.findElementByCssSelector("#pane").findElement(By.className("widget-pane-content-holder"));
            elementByXPath = elementByXPath.findElement(By.className("section-layout-root"));
            for (WebElement webElement : elementByXPath.findElements(By.xpath("./*"))) {
                System.out.println(webElement.getAttribute("class"));
                if (webElement.getAttribute("class").contains("section-layout")) {
                    for (WebElement element : webElement.findElements(By.xpath("./*"))) {
                        try {
                            if (element.getAttribute("class").contains("section-result")) {
                                jse.executeScript("arguments[0].click();", element);
                                Thread.sleep(100);

                                if (!checkList(places, driver.getCurrentUrl())) {
                                    places.add(driver.getCurrentUrl());
                                } else {
                                    System.out.println("Duplicate Found!");
                                    continue;
                                }
                                System.out.println(driver.getCurrentUrl());
                                stat.setText("Collecting : " + driver.getCurrentUrl());
                            }
                        } catch (Exception d) {
                            if (!resultPage.equalsIgnoreCase(driver.getCurrentUrl())) {
                                driver.navigate().back();
                                Thread.sleep(5000);
                                goBack = true;
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                }
            }
            if (!resultPage.equalsIgnoreCase(driver.getCurrentUrl()) && !goBack) {
                driver.navigate().back();
                Thread.sleep(5000);
            }
            System.out.println("next Page");

            try {
                elementByXPath1 = driver.findElementByXPath("/html/body/jsl/div[3]/div[8]/div[9]/div/div[1]/div/div");
                for (WebElement webElement : elementByXPath1.findElements(By.xpath("./*"))) {
                    if (webElement.getAttribute("class").contains("n7lv7yjyC35__root")) {
                        nextBtn = webElement.findElement(By.tagName("div")).findElement(By.className("n7lv7yjyC35__right"));
                        nextBtn.findElement(By.id("n7lv7yjyC35__section-pagination-button-next")).click();
                        Thread.sleep(10000);
                        resultPage = driver.getCurrentUrl();
                        break;
                    }
                }
            } catch (Exception d) {
                break;
            }
            if (places.size() > 100) {
                System.out.println("Link Collection Complate!");
                break;
            }
        }
        for (String b : places) {
            System.out.println(b);
        }
        System.out.println("done");
    }

    private List<GoogleMapCommon> findAccounts() {
        List<GoogleMapCommon> list = new ArrayList<>();
        GoogleMapCommon emails = null;
        String fileToParse = "EmailAccount.csv";
        BufferedReader fileReader = null;
        final String DELIMITER = ",";
        try {
            String line = "";
            fileReader = new BufferedReader(new FileReader(fileToParse));

            while ((line = fileReader.readLine()) != null) {
                emails = new GoogleMapCommon();
                String[] tokens = line.split(DELIMITER);
                try {
                    emails.setEmail(tokens[0].replace("\"", ""));
                    emails.setPassword(tokens[1].replace("\"", ""));
                    emails.setConformEmail(tokens[2].replace("\"", ""));
                    emails.setCountry(tokens[3].replace("\"", ""));
                    emails.setCity(tokens[4].replace("\"", ""));
                    if (emails.getEmail().equalsIgnoreCase("EMAIL")) {
                        continue;
                    }
                    list.add(emails);
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("check file. it can be empty");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                assert fileReader != null;
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    String findLevelUpElement;

    private void checkLevel(GoogleMapCommon gmc) throws InterruptedException, IOException {
        stat.setText("Checking Level for " + gmc.getEmail());
        driver.get("https://maps.google.com/localguides/home");
        Thread.sleep(5000);
        try {
            WebElement findLevelUpElement = driver.findElementByCssSelector(".profile-card-user");
            findLevelUpElement.getAttribute("innerText");
            LblLevelUp1.setText(findLevelUpElement.getAttribute("innerText").split("Level")[1]);
            gmc.setLevel(findLevelUpElement.getAttribute("innerText").split("Level")[1]);
        } catch (Exception e) {
            try {
                driver.findElementByCssSelector("#input-0").sendKeys(gmc.getCity());;
                Thread.sleep(1000);
                driver.findElementByCssSelector("li.ng-scope:nth-child(1)").click();
                Thread.sleep(1000);

                driver.findElementByCssSelector("md-checkbox.ng-pristine:nth-child(1) > div:nth-child(1)").click();
                driver.findElementByCssSelector("md-checkbox.ng-pristine:nth-child(2) > div:nth-child(1)").click();
                Thread.sleep(1000);

                driver.findElementByCssSelector(".large-btn").click();
                Thread.sleep(5000);

                WebElement findLevelUpElement = driver.findElementByCssSelector(".profile-card-user");
                findLevelUpElement.getAttribute("innerText");
                LblLevelUp1.setText(findLevelUpElement.getAttribute("innerText").split("Level")[1]);
                gmc.setLevel(findLevelUpElement.getAttribute("innerText").split("Level")[1]);
            } catch (Exception d) {
                LblLevelUp1.setText("Account Disable !");
            }
        }
        googleMapCommonList.add(gmc);
        writeCsv();
    }

    private void postReviews() throws InterruptedException {
        WebElement element1;
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        for (String place : places) {
            System.out.println(place);
            stat.setText("Posting review to " + place);
            try {
                jse = (JavascriptExecutor) driver;
                driver.get(place.split("@")[0]);
                driver.navigate().refresh();
            } catch (Exception f) {
                System.out.println("Malformed url");
//                continue;
            }
            Thread.sleep(5000);
            try {
                driver.findElementByCssSelector("#searchboxinput").sendKeys(Keys.ENTER);
                Thread.sleep(5000);
                element1 = driver.findElementByCssSelector("div.section-layout:nth-child(1)");
            } catch (Exception e) {
                Thread.sleep(5000);
                element1 = driver.findElementByCssSelector("div.section-layout:nth-child(1)");
            }
            System.out.println(element1.findElements(By.xpath("./*")).size()+"=================");
            for (WebElement webElement1 : element1.findElements(By.xpath("./*"))) {
                if (webElement1.getAttribute("innerText").contains("Write a review")) {
                    System.out.println(webElement1.getAttribute("innerText"));
                    jse.executeScript("arguments[0].click();", webElement1.findElement(By.tagName("button")));
                    Thread.sleep(5000);
                    driver.switchTo().frame(1);
                    driver.findElementByCssSelector("span.rating").findElements(By.xpath("./*")).get(4).click();
                    getReview();
                    driver.findElementByCssSelector(".review-text").sendKeys(message);
                    driver.findElementByCssSelector(".action-publish").findElement(By.tagName("div")).click();
                    Thread.sleep(1000);
                    try {
                        driver.findElementByCssSelector(".thanks-acstion-done-desktop > div:nth-child(1) > div:nth-child(1)").findElement(By.tagName("div")).click();
                    } catch (Exception e) {
                        driver.findElementByCssSelector(".thanks-action-done-desktop > div:nth-child(1)").click();
                    }
                    System.out.println("Review Ok......");
                    LblPostedReview.setText("" + count++);
                    break;
                }
            }
        }
    }

    private boolean checkList(List<String> places, String currentUrl) {
        return places.contains(currentUrl);
    }

    private void writeCsv() throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter("./Updated Accounts.csv"));
        List<String[]> csvRows = new LinkedList<String[]>();
        csvRows.add(new String[]{"Email", "Password", "Recover", "Country",
            "City", "Level"});

        for (GoogleMapCommon data : googleMapCommonList) {
            csvRows.add(new String[]{
                data.getEmail(),
                data.getPassword(),
                data.getConformEmail(),
                data.getCountry(),
                data.getCity(),
                data.getLevel()
            });
        }
        csvWriter.writeAll(csvRows);
        csvWriter.close();
    }
}
