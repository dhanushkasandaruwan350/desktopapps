/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oriant.hubSport;

import com.opencsv.CSVWriter;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import util.DriverInitializer;

/**
 *
 * @author DeadShot
 */
public class view extends javax.swing.JFrame {

    FirefoxDriver driver = null;
    private JavascriptExecutor executor;
    boolean running = false;
    String user = "";
    String pass = "";
    String code = "";
    String list = "";
    String sequence = "";

    int c = 100;

    public view() {
        initComponents();
        LblSendOne1.setVisible(false);
        auth.setVisible(false);
        this.setBackground(new Color(1.0f, 1.0f, 1.0f, 0f));
        readAccountInfo();
    }

    private void readAccountInfo() {
        String fileToParse = "account.csv";

        BufferedReader fileReader = null;
        final String DELIMITER = ",";
        try {
            String line = "";
            fileReader = new BufferedReader(new FileReader(fileToParse));

            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(DELIMITER);
                try {
                    user = tokens[0].replace("\"", "");
                    pass = tokens[1].replace("\"", "");
                    code = tokens[2].replace("\"", "");
                    this.list = tokens[3].replace("\"", "");
                    sequence = tokens[4].replace("\"", "");
                    txt_email.setText(user);
                    txt_pass.setText(pass);
                    txt_id.setText(code);
                    TxtListName.setText(list);
                    TxtSequenceName.setText(sequence);

                } catch (ArrayIndexOutOfBoundsException e) {
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        } finally {
            try {
                assert fileReader != null;
                fileReader.close();
            } catch (IOException | NullPointerException e) {
//                e.printStackTrace();
            }
        }
    }

    private void writeCsv() throws IOException {
        CSVWriter csvWriter = new CSVWriter(new FileWriter("account.csv"));
        List<String[]> csvRows = new LinkedList<String[]>();
        csvRows.add(new String[]{
            "User",
            "Password",
            "Code",
            "List",
            "Sequence"
        });
        csvRows.add(new String[]{user, pass, code, list, sequence});
        csvWriter.writeAll(csvRows);
        csvWriter.close();
    }

    private void start() {

        try {
            writeCsv();
        } catch (IOException ex) {
            Logger.getLogger(view.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (canStart()) {
            new Thread(() -> {
                try {
                    LblStatus.setText("STARTING!");
                    loginHubspot(user, pass);
                    LblStatus.setText("JOB DONE!");
                    driver.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    LblStatus.setText("JOB FAILED!");
                    driver.close();
                    JOptionPane.showMessageDialog(null, "Unexpected error in login section.", "AUTH SECTION FAILIER", JOptionPane.WARNING_MESSAGE);
                }
                running = false;
            }).start();
        } else {
            LblStatus.setText("Failed to start");
        }
    }

    private void loginHubspot(String email, String pass) throws InterruptedException {
        LblStatus.setText("Sending authorization info");
        List<String> list = new ArrayList<>();
        driver = new DriverInitializer().getFirefoxDriver();
        executor = (JavascriptExecutor) driver;

        driver.get("https://app.hubspot.com/getting-started/");
        Thread.sleep(10000);

        WebElement username = driver.findElementByCssSelector("#username");
        username.sendKeys(email);
        Thread.sleep(2000);

        WebElement password = driver.findElementByCssSelector("#password");
        password.sendKeys(pass);
        Thread.sleep(1000);

        WebElement loginBtn = driver.findElementByCssSelector("#loginBtn");
        loginBtn.click();
        Thread.sleep(10000);

        try {
            checkList(this.list, code);
        } catch (Exception e) {
            e.printStackTrace();
            LblStatus.setText("STOPPED");
//            driver.close();
            JOptionPane.showMessageDialog(null, "Web site not accepting this account.\n Check your credentials or Please try again after few minutes. \n or \n maybe this can be a source change in hubspot.", "AUTH SECTION FAILIER", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void checkList(String list, String id) throws InterruptedException {
        LblStatus.setText("Looking for list");
        String sequence = "";
        driver.get("https://app.hubspot.com/contacts/" + id + "/lists");
        Thread.sleep(10000);
        boolean found = false;
        while (!found) {
            for (WebElement tr : driver.findElementByCssSelector(".UITable__Table-sc-170ixfn-1 > tbody:nth-child(2)")
                    .findElements(By.xpath("./*"))) {
//                System.out.println(tr.findElements(By.xpath("./*")).get(1).getAttribute("innerText"));
                System.out.println("here " + tr.findElements(By.xpath("./*")).get(1).getAttribute("innerText").split("\n")[0]);
                if (tr.findElements(By.xpath("./*")).get(1).getAttribute("innerText").split("\n")[0].equalsIgnoreCase(list)) {
                    sequence = tr.findElements(By.xpath("./*")).get(1).findElement(By.tagName("a")).getAttribute("href");
                    try {
                        checkSequence(sequence);
                        found = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        LblStatus.setText("CAN NOT PROCEED");
//                        driver.close();
                        JOptionPane.showMessageDialog(null, "Enroll not found in email selection page.", "UNEXPECTED ERROR", JOptionPane.ERROR_MESSAGE);
                    }
                    break;
                }
            }

            try {

                WebElement next = driver.findElementByCssSelector(".private-paginator__forward-controls > button:nth-child(1)");
                if (next.getAttribute("aria-disabled").equalsIgnoreCase("true")) {
                    JOptionPane.showMessageDialog(null, "We don't find that list name.", "UNKNOWN LIST", JOptionPane.ERROR_MESSAGE);
                    break;

                }
                executor.executeScript("arguments[0].click();", next);
                Thread.sleep(5000);
            } catch (Exception f) {
                JOptionPane.showMessageDialog(null, "We don't find that list name.", "UNKNOWN LIST", JOptionPane.ERROR_MESSAGE);
                break;

            }
        }

    }

    private void checkSequence(String sequence) throws InterruptedException {
        LblStatus.setText("Checking sequence");
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                LblStatus.setText("Collecting 100 accounts");
                System.out.println("sending 100");
            } else {
                LblStatus.setText("Collecting 50 accounts");
                System.out.println("sending 50");
            }
            driver.get(sequence);
            Thread.sleep(10000);

            try {
                WebElement per_page = driver.findElementByCssSelector(".m-y-4");
                per_page = per_page.findElement(By.tagName("div")).findElement(By.tagName("button"));
                executor.executeScript("arguments[0].click();", per_page);
                Thread.sleep(500);

                if (i == 0) {
                    per_page = driver.findElementByCssSelector("#dropdown-menu-4").findElement(By.tagName("ul"));
                    per_page = per_page.findElements(By.xpath("./*")).get(2).findElement(By.tagName("button"));
                } else {
                    per_page = driver.findElementByCssSelector("#dropdown-menu-4").findElement(By.tagName("ul"));
                    per_page = per_page.findElements(By.xpath("./*")).get(1).findElement(By.tagName("button"));
                }
                executor.executeScript("arguments[0].click();", per_page);
                Thread.sleep(5000);

            } catch (Exception e) {
            }

            WebElement selectAll = driver.findElementByCssSelector("th.checkbox-cell > div:nth-child(1) > label:nth-child(1) > span:nth-child(1) > input:nth-child(1)");
            executor.executeScript("arguments[0].click();", selectAll);
            Thread.sleep(1000);

            WebElement moreOption = driver.findElementByCssSelector(".table-more-condensed").findElement(By.className("uiList")).findElements(By.xpath("./*")).get(3);
            moreOption = moreOption.findElement(By.tagName("button"));
            executor.executeScript("arguments[0].click();", moreOption);
            Thread.sleep(3000);

            WebElement enroll = null;
            try {
                enroll = driver.findElementByCssSelector("#dropdown-menu-5").findElement(By.tagName("ul"));
            } catch (Exception e) {
                try {
                    enroll = driver.findElementByCssSelector("#dropdown-menu-3").findElement(By.tagName("ul"));
                } catch (Exception g) {
                    try {
                        enroll = driver.findElementByCssSelector("#dropdown-menu-6").findElement(By.tagName("ul"));
                    } catch (Exception r) {
                        try {
                            enroll = driver.findElementByCssSelector("#dropdown-menu-67").findElement(By.tagName("ul"));
                        } catch (Exception d) {
                            enroll = driver.findElementByCssSelector("#dropdown-8").findElement(By.tagName("ul"));
                        }
                    }
                }
            }
            enroll = enroll.findElements(By.xpath("./*")).get(3).findElement(By.tagName("button"));
            executor.executeScript("arguments[0].click();", enroll);
            Thread.sleep(10000);

            try {
                selectSequence(this.sequence);
            } catch (Exception e) {
//                e.printStackTrace();
                LblStatus.setText("LIMIT EXCEEDED");
                driver.close();
                JOptionPane.showMessageDialog(null, "I think we are reached to the daiy limit.", "LIMIT EXCEEDED", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    private void selectSequence(String sequence) throws InterruptedException {

        LblStatus.setText("Sending sequence");
        boolean found = false;
        int count = 1;
        WebElement enroll = null;
        for (WebElement tr : driver.findElementByCssSelector(".sales-content-table > tbody:nth-child(3)").findElements(By.xpath("./*"))) {
            System.out.println(tr.findElements(By.xpath("./*")).get(0).getAttribute("innerText"));
            if (tr.getAttribute("innerText").split("\n")[0].equalsIgnoreCase(sequence)) {
                executor.executeScript("arguments[0].click();", tr);
                found = true;
                break;
            }
        }

        if (found) {
            while (true) {
                try {
                    Thread.sleep(5000);
                    enroll = driver.findElementByCssSelector(".private-button--primary");
                    executor.executeScript("arguments[0].click();", enroll);
                    LblStatus.setText("Send count " + count);
                    count++;
                } catch (Exception e) {
//                    e.printStackTrace();
                    break;
                }
            }
        }
        Thread.sleep(5000);
        for (WebElement e : driver.findElementByCssSelector(".uiList").findElements(By.xpath("./*"))) {
            if (e.getAttribute("innerText").contains("Remove from list")) {
                e.findElement(By.tagName("button")).click();

                Thread.sleep(2000);
                driver.findElementByCssSelector(".private-button--primary").click();
                break;
            }
        }
        Thread.sleep(5000);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        auth = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txt_pass = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_email = new javax.swing.JTextField();
        txt_id = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        BtnSetTimer1 = new javax.swing.JButton();
        pnlMain = new javax.swing.JPanel();
        TxtListName = new javax.swing.JTextField();
        ChkBoxReapt = new javax.swing.JCheckBox();
        LblSendTwo = new javax.swing.JLabel();
        LblSendOne1 = new javax.swing.JLabel();
        BtnSend = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        TxtSequenceName = new javax.swing.JTextField();
        hh = new javax.swing.JComboBox<>();
        mm = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        LblStatus = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        BtnSetTimer = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        auth.setBackground(new java.awt.Color(0, 65, 131));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Password : ");

        txt_pass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_passKeyReleased(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Email : ");

        txt_email.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_emailKeyReleased(evt);
            }
        });

        txt_id.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_idKeyReleased(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("ID :");

        BtnSetTimer1.setBackground(new java.awt.Color(0, 153, 51));
        BtnSetTimer1.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        BtnSetTimer1.setForeground(new java.awt.Color(255, 255, 255));
        BtnSetTimer1.setText("Done");
        BtnSetTimer1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnSetTimer1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnSetTimer1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout authLayout = new javax.swing.GroupLayout(auth);
        auth.setLayout(authLayout);
        authLayout.setHorizontalGroup(
            authLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(authLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(authLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(authLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_id, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                    .addComponent(txt_email)
                    .addComponent(txt_pass))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, authLayout.createSequentialGroup()
                .addContainerGap(162, Short.MAX_VALUE)
                .addComponent(BtnSetTimer1)
                .addGap(114, 114, 114))
        );
        authLayout.setVerticalGroup(
            authLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(authLayout.createSequentialGroup()
                .addContainerGap(37, Short.MAX_VALUE)
                .addGroup(authLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txt_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(authLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txt_pass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(authLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(BtnSetTimer1)
                .addContainerGap())
        );

        getContentPane().add(auth, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 30, 340, 200));

        pnlMain.setBackground(new java.awt.Color(0, 65, 131));
        pnlMain.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TxtListName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtListNameKeyReleased(evt);
            }
        });
        pnlMain.add(TxtListName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 130, 270, 33));

        ChkBoxReapt.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        ChkBoxReapt.setForeground(new java.awt.Color(255, 255, 255));
        ChkBoxReapt.setText("Reapt Everyday?");
        pnlMain.add(ChkBoxReapt, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, 180, 20));

        LblSendTwo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSendTwo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/hubSport/send2.png"))); // NOI18N
        pnlMain.add(LblSendTwo, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 283, 40, 30));

        LblSendOne1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSendOne1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/hubSport/send.png"))); // NOI18N
        pnlMain.add(LblSendOne1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 283, 40, 30));

        BtnSend.setBackground(new java.awt.Color(0, 153, 51));
        BtnSend.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        BtnSend.setForeground(new java.awt.Color(255, 255, 255));
        BtnSend.setText("Click To Send Now");
        BtnSend.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnSend.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnSendMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnSendMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnSendMouseExited(evt);
            }
        });
        pnlMain.add(BtnSend, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 280, 270, 40));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Sequence Name :");
        pnlMain.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 173, 100, -1));

        TxtSequenceName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TxtSequenceNameKeyReleased(evt);
            }
        });
        pnlMain.add(TxtSequenceName, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 190, 270, 33));

        hh.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        hh.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Hours", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "00" }));
        pnlMain.add(hh, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 360, 130, 33));

        mm.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        mm.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Min", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        pnlMain.add(mm, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 360, 120, 33));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("X");
        jLabel2.setBorder(null);
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel2MouseExited(evt);
            }
        });
        pnlMain.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(352, 2, 20, 20));

        LblStatus.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        LblStatus.setForeground(new java.awt.Color(102, 102, 102));
        LblStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblStatus.setText("Initialized");
        pnlMain.add(LblStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 450, 270, 20));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("List Name :");
        pnlMain.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 115, 70, -1));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/hubSport/timer.png"))); // NOI18N
        pnlMain.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 400, 30, 30));

        BtnSetTimer.setBackground(new java.awt.Color(0, 153, 51));
        BtnSetTimer.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        BtnSetTimer.setForeground(new java.awt.Color(255, 255, 255));
        BtnSetTimer.setText("Set Timer");
        BtnSetTimer.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BtnSetTimer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                BtnSetTimerMouseClicked(evt);
            }
        });
        pnlMain.add(BtnSetTimer, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 400, 160, 35));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/hubSport/logo1.png"))); // NOI18N
        pnlMain.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(146, 480, 70, 60));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Or Select Start Time", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Times New Roman", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N
        pnlMain.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 340, 290, 110));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/hubSport/Logo.png"))); // NOI18N
        pnlMain.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(26, 20, 320, -1));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("@ Native Solutions");
        pnlMain.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(128, 540, 110, -1));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/hubSport/Entypo_d83d(0)_32.png"))); // NOI18N
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });
        pnlMain.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(321, 80, 30, 30));

        getContentPane().add(pnlMain, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        System.exit(0);        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseEntered
        jLabel2.setForeground(Color.RED); // TODO add your handling code here:
    }//GEN-LAST:event_jLabel2MouseEntered

    private void jLabel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseExited
        jLabel2.setForeground(Color.WHITE);        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel2MouseExited

    private void BtnSendMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnSendMouseEntered
        BtnSend.setBackground(Color.blue);
        BtnSend.setForeground(Color.GREEN);
        LblSendTwo.setVisible(false);
        LblSendOne1.setVisible(true);
    }//GEN-LAST:event_BtnSendMouseEntered

    private void BtnSendMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnSendMouseExited
        BtnSend.setBackground((new Color(0, 153, 51)));
        BtnSend.setForeground(Color.WHITE);
        LblSendOne1.setVisible(false);
        LblSendTwo.setVisible(true);
    }//GEN-LAST:event_BtnSendMouseExited

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        auth.setVisible(true);
    }//GEN-LAST:event_jLabel3MouseClicked

    private void BtnSetTimer1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnSetTimer1MouseClicked
        auth.setVisible(false);
    }//GEN-LAST:event_BtnSetTimer1MouseClicked

    private void BtnSendMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnSendMouseClicked

        start();

    }//GEN-LAST:event_BtnSendMouseClicked

    private void BtnSetTimerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnSetTimerMouseClicked
        try {
            setTimer();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Can not set the timer", "TIMER ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_BtnSetTimerMouseClicked

    private void txt_emailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_emailKeyReleased
        user = txt_email.getText();
    }//GEN-LAST:event_txt_emailKeyReleased

    private void txt_passKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_passKeyReleased
        pass = txt_pass.getText();
    }//GEN-LAST:event_txt_passKeyReleased

    private void txt_idKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idKeyReleased
        code = txt_id.getText();
    }//GEN-LAST:event_txt_idKeyReleased

    private void TxtListNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtListNameKeyReleased
        list = TxtListName.getText();
    }//GEN-LAST:event_TxtListNameKeyReleased

    private void TxtSequenceNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtSequenceNameKeyReleased
        sequence = TxtSequenceName.getText();
    }//GEN-LAST:event_TxtSequenceNameKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new view().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnSend;
    private javax.swing.JButton BtnSetTimer;
    private javax.swing.JButton BtnSetTimer1;
    private javax.swing.JCheckBox ChkBoxReapt;
    private javax.swing.JLabel LblSendOne1;
    private javax.swing.JLabel LblSendTwo;
    private javax.swing.JLabel LblStatus;
    private javax.swing.JTextField TxtListName;
    private javax.swing.JTextField TxtSequenceName;
    private javax.swing.JPanel auth;
    private javax.swing.JComboBox<String> hh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JComboBox<String> mm;
    private javax.swing.JPanel pnlMain;
    private javax.swing.JTextField txt_email;
    private javax.swing.JTextField txt_id;
    private javax.swing.JTextField txt_pass;
    // End of variables declaration//GEN-END:variables

    private boolean canStart() {
        LblStatus.setText("Checking neccesory arguments");
        if (running == true) {
            JOptionPane.showMessageDialog(null, "Bot is still working on last sequence.", "REQUEST REJECTED", JOptionPane.INFORMATION_MESSAGE);
            return false;
        }
        if (TxtListName.getText().length() > 1 && TxtSequenceName.getText().length() > 1) {
            if (txt_email.getText().length() > 3 && txt_id.getText().length() > 3 && txt_pass.getText().length() > 5) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Please check your account details befor continue", "INVALID AUTHORIZATION DETAILS", JOptionPane.WARNING_MESSAGE);
                return false;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please check target list and sequence", "INVALID ARGUMENTS", JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }

    private void setTimer() throws ParseException {
        LblStatus.setText("Setting Timer");
        final LocalDateTime now = LocalDateTime.now();
        final LocalDateTime ninePMToday = LocalDateTime.now()
                .withHour(Integer.parseInt(hh.getSelectedItem().toString()))
                .withMinute(Integer.parseInt(mm.getSelectedItem().toString()))
                .withSecond(0)
                .withNano(0);
        final ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        ses.scheduleAtFixedRate(() -> start(),
                now.until(ninePMToday, ChronoUnit.MILLIS),
                TimeUnit.DAYS.toMillis(1),
                TimeUnit.MILLISECONDS);
        LblStatus.setText("Timer Set");
    }
}
