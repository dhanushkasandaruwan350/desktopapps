/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oriant.adobestock;

import com.opencsv.CSVWriter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import static org.apache.commons.math3.fitting.leastsquares.LeastSquaresFactory.model;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.w3c.dom.html.HTMLCollection;
import util.DriverInitializer;

/**
 *
 * @author DeadShot
 */
public class view extends javax.swing.JFrame {

    /**
     * Creates new form view
     */
    private FirefoxDriver driver = null;
    AdobeCommon adobeCommon = null;
    List<AdobeCommon> adobeCommonList = new ArrayList<>();
    JLabel imageLable = null;
    JTable table = new JTable();
    JTable table1;

    public view() {
        this.setUndecorated(true);
//        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setVisible(true);
        initComponents();

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int x = (int) toolkit.getScreenSize().getWidth();
        int y = (int) toolkit.getScreenSize().getHeight();
        this.setSize(x, y);

    }

    public void initialise() throws Exception {
        driver = new DriverInitializer().getFirefoxDriver();
        scrape("https://stock.adobe.com/search?filters%5Bcontent_type%3Aphoto%5D=1&filters%5Bcontent_type%3Aillustration%5D=1&filters%5Bcontent_type%3Azip_vector%5D=1&filters%5Bcontent_type%3Avideo%5D=1&filters%5Bcontent_type%3Atemplate%5D=1&filters%5Bcontent_type%3A3d%5D=1&filters%5Bcontent_type%3Aimage%5D=1&order=relevance&safe_search=1&search_type=usertyped&limit=100&search_page=1&acp=&aco=wedding+d&get_facets=0");
    }

    private void scrape(String link) throws InterruptedException, MalformedURLException {
        driver.get(link);
        List<String> list = new ArrayList<>();
        WebElement inputText = driver.findElementByXPath("/html/body/div[2]/div[1]/div[2]/div[1]/div/div[2]/div/form/div/div/div[3]/div/input");
        inputText.sendKeys(keyword.getText());
        inputText.sendKeys(Keys.ENTER);
        WebElement allImages = driver.findElementByXPath("/html/body/div[2]/div[2]/div/div/div[2]/div[3]");
        WebElement searchResalt = allImages.findElement(By.className("flex-1")).findElement(By.tagName("div")).findElement(By.tagName("div")).findElement(By.id("search-results"));
        Thread.sleep(5000);
        while (true) {
            for (WebElement element : searchResalt.findElements(By.xpath("./*"))) {
                WebElement imageDiv = element.findElement(By.tagName("div")).findElement(By.tagName("a"));
                String url = imageDiv.getAttribute("href");
//                System.out.println("------------------------------------------------------------------");
                System.out.println("Image Url :" + url);
//                System.out.println("------------------------------------------------------------------");
                list.add(url);
            }
            WebElement pagination = allImages.findElement(By.id("pagination-element")).findElement(By.className("pagination-wrapper")).findElement(By.tagName("form")).findElement(By.tagName("ul"));
            for (WebElement element : pagination.findElements(By.xpath("./*"))) {
                if (element.getAttribute("class").contains("pagination-next")) {
                    element.findElement(By.tagName("a")).click();
                    System.out.println("<<======== Next Page =======>>");
                    Thread.sleep(5000);
                }
            }
            if (list.size() > 20) {
                break;
            }
        }
        screpeInnerPage(list);
    }

    private void screpeInnerPage(List<String> list) throws InterruptedException {
        for (String s : list) {
            driver.get(s);
            Thread.sleep(2000);
            WebElement imageContainer = driver.findElementByXPath("/html/body/div[1]/div[2]/div[2]/div/div/div");
            WebElement colorCodeContainer = imageContainer.findElement(By.className("row")).findElements(By.tagName("div")).get(0);
            WebElement nameContainer = imageContainer.findElement(By.className("row")).findElement(By.className("details-right-panel"));
            WebElement colorCode = colorCodeContainer.findElement(By.className("margin-bottom-small")).findElement(By.tagName("span")).findElement(By.tagName("a"));
            String textColor = colorCode.getAttribute("innerText");
            String urlColor = colorCode.getAttribute("href");
            WebElement elementName = nameContainer.findElement(By.tagName("div")).findElement(By.tagName("div")).findElement(By.tagName("div"));
            String imageName = "";
            for (WebElement element : elementName.findElements(By.xpath("./*"))) {
                if (element.getAttribute("class").contains("margin-bottom-medium")) {
                    try {
                        WebElement a = element.findElement(By.tagName("h1")).findElement(By.tagName("a"));
                        if (a.getAttribute("innerText").contains("See More")) {
                            a.click();
                            Thread.sleep(1000);
                            imageName = element.findElement(By.tagName("h1")).getText();
                            System.out.println("Image Name : " + imageName);
                        }
                    } catch (Exception e) {
                        try {
                            imageName = element.findElement(By.tagName("h2")).findElement(By.tagName("span")).getAttribute("innerText");
                            System.out.println("Image Name : " + imageName);
                        } catch (NoSuchElementException exception) {
                            imageName = element.findElement(By.tagName("h1")).getText();
                            System.out.println("Image Name : " + imageName);
                        }
                    }
                }
            }
            System.out.println("Image Url :" + s);
            System.out.println("Color Code : " + textColor);
            System.out.println("Color Url : " + urlColor);
            System.out.println("===========================================================================================");

            adobeCommon = new AdobeCommon();
            adobeCommon.setImageUrl(s);
            adobeCommon.setImageCode(textColor);
            adobeCommon.setImageCodeUrl(urlColor);
            adobeCommon.setImageDescription(imageName);
            adobeCommonList.add(adobeCommon);

            try {
                createExcelFile(adobeCommonList);
            } catch (IOException e) {
                e.printStackTrace();
            }
            break;
        }

        table.setDefaultRenderer(Object.class, new Renderer());

        setLayout(new FlowLayout());

        String[] columnNames = {"ColumnOne", "ColumnOne", "ColumnOne", "ColumnOne"};

        //JLabel lable = new JLabel();
        ImageIcon mylable = new ImageIcon("https://www.w3schools.com/images/w3schools_green.jpg");
        //lable.setIcon(new ImageIcon("https://www.w3schools.com/images/w3schools_green.jpg"));

        Object[][] data = {
            {mylable, "TextData", "TextData", "TextData"},
            {"TextData", "TextData", "TextData", "TextData"},
            {"TextData", "TextData", "TextData", "TextData"},
            {"TextData", "TextData", "TextData", "TextData"},
        };
        // JLabel label = new JLabel(new ImageIcon("https://image.shutterstock.com/image-photo/beautiful-water-drop-on-dandelion-260nw-789676552.jpg"));
        DefaultTableModel model = new DefaultTableModel(data, columnNames);
        table = new JTable(model) {
            public Class getColumnClass(int columnNames) {
                return ImageIcon.class;
            }
        };
        table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(new Dimension(500, 200));
        table.setFillsViewportHeight(true);

        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);

        table.setRowHeight(200);
        table.setSize(1300, 600);
        table.setVisible(true);
        PnlScroll.add(table);

    }

    private void createExcelFile(List<AdobeCommon> adobeCommonList) throws IOException {
        System.out.println("writing");
        CSVWriter csvWriter = new CSVWriter(new FileWriter("./adobe.csv"));
        List<String[]> csvRows = new LinkedList<String[]>();
        csvRows.add(new String[]{
            "",
            "AdobeStock ID",
            "Title",
            "Description",
            "Word Count",
            "AdobeStock URL"
        });

        int count = 1;
        for (AdobeCommon data : adobeCommonList) {
            csvRows.add(new String[]{
                count + "",
                data.getImageCode(),
                data.getImageDescription(),
                data.getImageDescription(),
                data.getWordCount(),
                data.getImageUrl()
            });
            count++;
        }
        csvWriter.writeAll(csvRows);
        csvWriter.close();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMain = new javax.swing.JPanel();
        LblMinimize = new javax.swing.JLabel();
        LblClose = new javax.swing.JLabel();
        LblHeader = new javax.swing.JLabel();
        LblFooter = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        keyword = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        LblFileBtnCloseOne = new javax.swing.JLabel();
        LblDownloadBtnOne = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        LblFileBtnOne = new javax.swing.JLabel();
        LblDeleteBtnOne = new javax.swing.JLabel();
        LblSearchBtnOne = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        jSpinner2 = new javax.swing.JSpinner();
        LblContent = new javax.swing.JLabel();
        images = new javax.swing.JScrollPane();
        PnlScroll = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pnlMain.setBackground(new java.awt.Color(102, 102, 102));
        pnlMain.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 153), 4));
        pnlMain.setPreferredSize(new java.awt.Dimension(1366, 768));
        pnlMain.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        LblMinimize.setBackground(new java.awt.Color(204, 204, 0));
        LblMinimize.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        LblMinimize.setForeground(new java.awt.Color(255, 255, 255));
        LblMinimize.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblMinimize.setText("-");
        LblMinimize.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0)));
        LblMinimize.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblMinimize.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblMinimizeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblMinimizeMouseExited(evt);
            }
        });
        pnlMain.add(LblMinimize, new org.netbeans.lib.awtextra.AbsoluteConstraints(1315, 10, 20, 20));

        LblClose.setBackground(new java.awt.Color(153, 0, 0));
        LblClose.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        LblClose.setForeground(new java.awt.Color(255, 255, 255));
        LblClose.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblClose.setText("X");
        LblClose.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0)));
        LblClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblCloseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblCloseMouseExited(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblCloseMouseClicked(evt);
            }
        });
        pnlMain.add(LblClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(1338, 10, 20, 20));

        LblHeader.setBackground(new java.awt.Color(0, 51, 51));
        LblHeader.setOpaque(true);
        pnlMain.add(LblHeader, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 4, 1358, 40));

        LblFooter.setBackground(new java.awt.Color(153, 0, 0));
        LblFooter.setOpaque(true);
        pnlMain.add(LblFooter, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 739, 1358, 20));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel1.setText("Keyword               :");
        pnlMain.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 130, 30));

        pnlMain.add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 60, 180, 30));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel2.setText("Folder Name        :");
        pnlMain.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 130, 30));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel3.setText("Name Start From :");
        pnlMain.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 100, 130, 30));
        pnlMain.add(keyword, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 60, 240, 30));

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        pnlMain.add(jTextField4, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 100, 240, 30));

        LblFileBtnCloseOne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblFileBtnCloseOne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/adobestock/file_del.png"))); // NOI18N
        LblFileBtnCloseOne.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        LblFileBtnCloseOne.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblFileBtnCloseOne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblFileBtnCloseOneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblFileBtnCloseOneMouseExited(evt);
            }
        });
        pnlMain.add(LblFileBtnCloseOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 60, 40, 40));

        LblDownloadBtnOne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblDownloadBtnOne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/adobestock/download.png"))); // NOI18N
        LblDownloadBtnOne.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        LblDownloadBtnOne.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblDownloadBtnOne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblDownloadBtnOneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblDownloadBtnOneMouseExited(evt);
            }
        });
        pnlMain.add(LblDownloadBtnOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 60, 40, 40));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        jLabel6.setText("Project    :");
        pnlMain.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 60, 70, 30));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Add / Load");
        jLabel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel7MouseExited(evt);
            }
        });
        pnlMain.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(894, 100, 290, 30));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Config Search");
        jLabel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel8MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel8MouseExited(evt);
            }
        });
        pnlMain.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1076, 62, 100, 25));

        LblFileBtnOne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblFileBtnOne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/adobestock/file.png"))); // NOI18N
        LblFileBtnOne.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        LblFileBtnOne.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblFileBtnOne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblFileBtnOneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblFileBtnOneMouseExited(evt);
            }
        });
        pnlMain.add(LblFileBtnOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 60, 40, 40));

        LblDeleteBtnOne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblDeleteBtnOne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/adobestock/delete.png"))); // NOI18N
        LblDeleteBtnOne.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        LblDeleteBtnOne.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblDeleteBtnOne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblDeleteBtnOneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblDeleteBtnOneMouseExited(evt);
            }
        });
        pnlMain.add(LblDeleteBtnOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 60, 40, 40));

        LblSearchBtnOne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblSearchBtnOne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/adobestock/search.png"))); // NOI18N
        LblSearchBtnOne.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        LblSearchBtnOne.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        LblSearchBtnOne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblSearchBtnOneMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                LblSearchBtnOneMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LblSearchBtnOneMouseEntered(evt);
            }
        });
        pnlMain.add(LblSearchBtnOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 60, 40, 40));
        pnlMain.add(jSpinner1, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 100, 50, -1));
        pnlMain.add(jSpinner2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 60, 180, -1));

        LblContent.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlMain.add(LblContent, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 1344, 90));

        images.setBackground(new java.awt.Color(102, 102, 102));
        images.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        javax.swing.GroupLayout PnlScrollLayout = new javax.swing.GroupLayout(PnlScroll);
        PnlScroll.setLayout(PnlScrollLayout);
        PnlScrollLayout.setHorizontalGroup(
            PnlScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1323, Short.MAX_VALUE)
        );
        PnlScrollLayout.setVerticalGroup(
            PnlScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 603, Short.MAX_VALUE)
        );

        images.setViewportView(PnlScroll);

        pnlMain.add(images, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 1325, 580));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void LblFileBtnOneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblFileBtnOneMouseEntered
        LblFileBtnOne.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));
    }//GEN-LAST:event_LblFileBtnOneMouseEntered

    private void LblFileBtnOneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblFileBtnOneMouseExited
        LblFileBtnOne.setBorder(BorderFactory.createLineBorder(Color.BLACK));
    }//GEN-LAST:event_LblFileBtnOneMouseExited

    private void LblFileBtnCloseOneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblFileBtnCloseOneMouseEntered
        LblFileBtnCloseOne.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));        // TODO add your handling code here:
    }//GEN-LAST:event_LblFileBtnCloseOneMouseEntered

    private void LblFileBtnCloseOneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblFileBtnCloseOneMouseExited
        LblFileBtnCloseOne.setBorder(BorderFactory.createLineBorder(Color.BLACK));          // TODO add your handling code here:
    }//GEN-LAST:event_LblFileBtnCloseOneMouseExited

    private void LblSearchBtnOneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSearchBtnOneMouseEntered
        LblSearchBtnOne.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));  // TODO add your handling code here:
    }//GEN-LAST:event_LblSearchBtnOneMouseEntered

    private void LblSearchBtnOneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSearchBtnOneMouseExited
        LblSearchBtnOne.setBorder(BorderFactory.createLineBorder(Color.BLACK));          // TODO add your handling code here:
    }//GEN-LAST:event_LblSearchBtnOneMouseExited

    private void LblDownloadBtnOneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblDownloadBtnOneMouseEntered
        LblDownloadBtnOne.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));          // TODO add your handling code here:
    }//GEN-LAST:event_LblDownloadBtnOneMouseEntered

    private void LblDownloadBtnOneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblDownloadBtnOneMouseExited
        LblDownloadBtnOne.setBorder(BorderFactory.createLineBorder(Color.BLACK));          // TODO add your handling code here:
    }//GEN-LAST:event_LblDownloadBtnOneMouseExited

    private void LblDeleteBtnOneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblDeleteBtnOneMouseEntered
        LblDeleteBtnOne.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));          // TODO add your handling code here:
    }//GEN-LAST:event_LblDeleteBtnOneMouseEntered

    private void LblDeleteBtnOneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblDeleteBtnOneMouseExited
        LblDeleteBtnOne.setBorder(BorderFactory.createLineBorder(Color.BLACK));          // TODO add your handling code here:
    }//GEN-LAST:event_LblDeleteBtnOneMouseExited

    private void LblCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblCloseMouseClicked
        System.exit(0);
    }//GEN-LAST:event_LblCloseMouseClicked

    private void LblCloseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblCloseMouseEntered
        LblClose.setOpaque(true);
        repaint();
    }//GEN-LAST:event_LblCloseMouseEntered

    private void LblCloseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblCloseMouseExited
        LblClose.setOpaque(false);
        repaint();
    }//GEN-LAST:event_LblCloseMouseExited

    private void LblMinimizeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblMinimizeMouseEntered
        LblMinimize.setOpaque(true);
        repaint();
    }//GEN-LAST:event_LblMinimizeMouseEntered

    private void LblMinimizeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblMinimizeMouseExited
        LblMinimize.setOpaque(false);
        repaint();// TODO add your handling code here:
    }//GEN-LAST:event_LblMinimizeMouseExited

    private void jLabel7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseEntered
        jLabel7.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));
        jLabel7.setForeground(Color.GREEN);// TODO add your handling code here:
    }//GEN-LAST:event_jLabel7MouseEntered

    private void jLabel7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseExited
        jLabel7.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jLabel7.setForeground(Color.WHITE);// TODO add your handling code here:
    }//GEN-LAST:event_jLabel7MouseExited

    private void jLabel8MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseEntered
        jLabel8.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));
        jLabel8.setForeground(Color.GREEN);
    }//GEN-LAST:event_jLabel8MouseEntered

    private void jLabel8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseExited
        jLabel8.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jLabel8.setForeground(Color.WHITE);
    }//GEN-LAST:event_jLabel8MouseExited

    private void LblSearchBtnOneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblSearchBtnOneMouseClicked
        new Thread(() -> {
            try {
                initialise();
            } catch (Exception ex) {
                Logger.getLogger(view.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }//GEN-LAST:event_LblSearchBtnOneMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(view.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new view().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LblClose;
    private javax.swing.JLabel LblContent;
    private javax.swing.JLabel LblDeleteBtnOne;
    private javax.swing.JLabel LblDownloadBtnOne;
    private javax.swing.JLabel LblFileBtnCloseOne;
    private javax.swing.JLabel LblFileBtnOne;
    private javax.swing.JLabel LblFooter;
    private javax.swing.JLabel LblHeader;
    private javax.swing.JLabel LblMinimize;
    private javax.swing.JLabel LblSearchBtnOne;
    private javax.swing.JPanel PnlScroll;
    private javax.swing.JScrollPane images;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField keyword;
    private javax.swing.JPanel pnlMain;
    // End of variables declaration//GEN-END:variables
}
