package knowem;

import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultCaret;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import util.DriverInitializer;
import util.Speech;
import util.Trial;

public class NewJFrame extends javax.swing.JFrame {

    boolean start = false;
    FirefoxDriver driver;
    List<String> availableSites;
    CSVWriter account;
    String date;
    String stat = "";
    Speech speech;

    public NewJFrame() {
        initComponents();
        this.setLocationRelativeTo(null);
        date = new Trial().getDate();
        DefaultCaret caret = (DefaultCaret) jTextArea1.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        speech = new Speech();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        acc_name_txt = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        acc_email_txt = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        name_txt = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        pw = new javax.swing.JPasswordField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(33, 50, 59));

        jLabel1.setFont(new java.awt.Font("Sarai", 3, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(219, 228, 231));
        jLabel1.setText("Knowem");

        jSeparator1.setBackground(new java.awt.Color(88, 94, 97));
        jSeparator1.setForeground(new java.awt.Color(220, 228, 231));

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(57, 67, 67), 2));

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        jTextArea1.setForeground(new java.awt.Color(45, 64, 75));
        jTextArea1.setRows(5);
        jTextArea1.setText("\n");
        jTextArea1.setOpaque(false);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 606, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        jLabel2.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(140, 153, 159));
        jLabel2.setText("Account Name : ");

        acc_name_txt.setBackground(new java.awt.Color(65, 76, 82));
        acc_name_txt.setForeground(new java.awt.Color(244, 244, 244));

        jLabel3.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(140, 153, 159));
        jLabel3.setText("Account Email : ");

        acc_email_txt.setBackground(new java.awt.Color(65, 76, 82));
        acc_email_txt.setForeground(new java.awt.Color(244, 244, 244));

        jLabel4.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(140, 153, 159));
        jLabel4.setText("Name : ");

        name_txt.setBackground(new java.awt.Color(65, 76, 82));
        name_txt.setForeground(new java.awt.Color(244, 244, 244));

        jLabel5.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(140, 153, 159));
        jLabel5.setText("Password : ");

        pw.setBackground(new java.awt.Color(65, 76, 82));
        pw.setForeground(new java.awt.Color(244, 244, 244));
        pw.setToolTipText("Please Include Numbers, Symbols, Capital Letters, and Lower-Case Letters: Use a mix of different types of characters to make the password harder to crack.");

        jLabel6.setFont(new java.awt.Font("SansSerif", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(216, 219, 220));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("START");
        jLabel6.setBorder(javax.swing.BorderFactory.createMatteBorder(3, 1, 3, 1, new java.awt.Color(35, 55, 56)));
        jLabel6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("SansSerif", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(216, 219, 220));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Welcome Back!");
        jLabel7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(acc_name_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3)
                                    .addComponent(acc_email_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4)
                                    .addComponent(name_txt, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5)
                                    .addComponent(pw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 634, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(112, 112, 112)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 142, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {name_txt, pw});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(acc_name_txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(acc_email_txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(name_txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pw, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(19, Short.MAX_VALUE))))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {name_txt, pw});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        if (start) {
            JOptionPane.showMessageDialog(rootPane, "There is an ongoing task. \n if you want to new round please restart the app.", "Already Running!", JOptionPane.WARNING_MESSAGE);
        } else {
            new Thread(() -> {
                try {
                    status("Starting..");
                    startApp();
                } catch (InterruptedException ex) {
                    Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
        }
    }//GEN-LAST:event_jLabel6MouseClicked

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField acc_email_txt;
    private javax.swing.JTextField acc_name_txt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField name_txt;
    private javax.swing.JPasswordField pw;
    // End of variables declaration//GEN-END:variables

    private void startApp() throws InterruptedException {
        status("initializing drivers...");
        initializeDrivers();
        status("searching on knowem...");
        searchOnKnowem();
        status("checking available links...");
        checkLinks();
    }

    private void initializeDrivers() {
        try {
            driver = new DriverInitializer().getFirefoxDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Can not execute drivers. \n Please make sure geckodriver or geckodriver.exe exist in your project folder", "Driver Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void searchOnKnowem() {
        availableSites = new ArrayList<>();
        try {
            driver.get("https://knowem.com/checksocialnames.php?u=" + name_txt.getText());
            Thread.sleep(5000);

            for (WebElement div : driver.findElementByCssSelector("fieldset.siteList:nth-child(4)")
                    .findElements(By.tagName("div"))) {
                try {
                    if (div.findElement(By.className("popup")).getAttribute("innerText").equalsIgnoreCase("Available")) {
                        System.out.println(div.findElement(By.className("popup"))
                                .findElement(By.tagName("a")).getAttribute("href") + "-"
                                + div.findElement(By.className("popup")).getAttribute("innerText"));
                        availableSites.add(div.findElement(By.className("popup"))
                                .findElement(By.tagName("a")).getAttribute("href"));
                        status(div.findElement(By.className("popup"))
                                .findElement(By.tagName("a")).getAttribute("href"));
                    }
                } catch (Exception d) {
                }
            }
        } catch (InterruptedException ex) {
            speech.unexpectedError();
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void checkLinks() throws InterruptedException {
        for (String url : availableSites) {
            if (url.contains("www.moonfruit.com")) {
                try {
                    status("Registering in www.moonfruit.com");
                    registerMoonFruit();
                    status("www.moonfruit.com registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("www.moonfruit.com registration failed!");
                }
            } else if (url.contains("https://wordpress.com/")) {
                try {
                    status("Registering in https://wordpress.com/");
                    registerWordpress();
                    status("https://wordpress.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://wordpress.com/ registration failed!");
                }
            } else if (url.contains("https://www.tumblr.com")) {
                try {
                    status("Registering in https://www.tumblr.com");
                    registerTumblr();
                    status("https://www.tumblr.com registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://www.tumblr.com registration failed!");
                }
            } else if (url.contains("https://www.blogger.com")) {
//try{
//                registerBlogger();
//}catch(Exception v){}
            } else if (url.contains("https://www.linkedin.com/")) {
                try {
                    status("Registering in https://www.linkedin.com/");
                    registerLinkedin();
                    status("https://www.linkedin.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://www.linkedin.com/ registration failed!");
                }
            } else if (url.contains("http://moblog.net/")) {
                try {
                    status("Registering in http://moblog.net/");
                    registerMoblog();
                    status("http://moblog.net/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("http://moblog.net/ registration failed!");
                }
            } else if (url.contains("https://en.over-blog.com/")) {
                try {
                    status("Registering in https://en.over-blog.com/");
                    registerOverBlog();
                    status("https://en.over-blog.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://en.over-blog.com/ registration failed!");
                }
            } else if (url.contains("https://www.livejournal.com/")) {
                try {
                    status("Registering in https://www.livejournal.com/");
                    registerLiveJournal();
                    status("https://www.livejournal.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://www.livejournal.com/ registration failed!");
                }
            } else if (url.contains("https://www.weebly.com/")) {
                try {
                    status("Registering in https://www.weebly.com/");
                    registerWeebly();
                    status("https://www.weebly.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://www.weebly.com/ registration failed!");
                }
            } else if (url.contains("https://issuu.com/")) {
                try {
                    status("Registering in https://issuu.com/");
                    registerIssuu();
                    status("https://issuu.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://issuu.com/ registration failed!");
                }
            } else if (url.contains("https://www.buzzfeed.com/")) {
                try {
                    status("Registering in https://www.buzzfeed.com/");
                    registerBuzzfeed();
                    status("https://www.buzzfeed.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://www.buzzfeed.com/ registration failed!");
                }
            } else if (url.contains("https://www.reddit.com/")) {
                try {
                    status("Registering in https://www.reddit.com/");
                    registerReddit();
                    status("https://www.reddit.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://www.reddit.com/ registration failed!");
                }
            } else if (url.contains("https://vimeo.com/")) {
                try {
                    status("Registering in https://vimeo.com/");
                    registerVimeo();
                    status("https://vimeo.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://vimeo.com/ registration failed!");
                }
            } else if (url.contains("https://mix.com/")) {
                try {
                    status("Registering in https://mix.com/");
                    registerMix();
                    status("https://mix.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://mix.com/ registration failed!");
                }
            } else if (url.contains("https://en-gb.facebook.com/")) {
                try {
                    status("Registering in https://en-gb.facebook.com/");
                    registerFacebook();
                    status("https://en-gb.facebook.com/ registration success!");
                } catch (Exception v) {
                    v.printStackTrace();
                    status("https://en-gb.facebook.com/ registration failed!");
                }
            }
        }
//        driver.close();
        status("Done!");
        speech.speak("Registrations are done.");
        speech.stop();
    }

    private void registerMoonFruit() throws Exception {
        driver.get("https://www.moonfruit.com");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Domains'])[3]/following::span[1]")).click();
        driver.findElement(By.id("signup-username")).click();
        driver.findElement(By.id("signup-username")).clear();
        driver.findElement(By.id("signup-username")).sendKeys(acc_name_txt.getText() + "A");
        driver.findElement(By.id("signup-email")).clear();
        driver.findElement(By.id("signup-email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("signup-password")).clear();
        driver.findElement(By.id("signup-password")).sendKeys(pw.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Remember me'])[2]/following::span[1]")).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='privacy policy'])[1]/following::input[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='privacy policy'])[1]/following::input[1]")).clear();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='privacy policy'])[1]/following::input[1]")).sendKeys(name_txt.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Change'])[1]/following::input[1]")).clear();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Change'])[1]/following::input[1]")).sendKeys(name_txt.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::span[1]")).click();
        writeToCsv("https://www.moonfruit.com/", name_txt.getText(), acc_name_txt.getText() + "A", acc_email_txt.getText(), pw.getText());
    }

    private void writeToCsv(String site, String name, String accName, String email, String pw) {
        try {
            account = new CSVWriter(new FileWriter("created_accounts.csv", true));
            List<String[]> csvRows = new LinkedList<String[]>();
            csvRows.add(new String[]{site, email, accName, pw, name, date});
            account.writeAll(csvRows);
            account.close();
        } catch (IOException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void registerWordpress() throws Exception {
        driver.get("https://wordpress.com/start/user");
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys(acc_name_txt.getText());
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys(pw.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Terms of Service'])[1]/following::button[1]")).click();
        writeToCsv("https://wordpress.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerTumblr() throws Exception {
        driver.get("https://www.tumblr.com/register");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Available usernames'])[1]/following::span[1]")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.id("signup_email")).clear();
        driver.findElement(By.id("signup_email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("signup_password")).clear();
        driver.findElement(By.id("signup_password")).sendKeys(pw.getText());
        try {
            driver.findElement(By.id("signup_username")).click();
            driver.findElement(By.id("signup_username")).clear();
            driver.findElement(By.id("signup_username")).sendKeys(acc_name_txt.getText());
        } catch (ElementNotInteractableException enie) {
            driver.findElementByCssSelector("li.popover_menu_item:nth-child(1)").click();
        }
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/following::span[1]")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            driver.findElement(By.id("signup_age")).clear();
        } catch (Exception f) {
            Thread.sleep(5000);
        }
        driver.findElement(By.id("signup_age")).sendKeys("21");
        driver.findElement(By.id("signup_tos")).click();
        driver.findElementByCssSelector("#signup_forms_submit").click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        speech.resolveCaptcha();
        JOptionPane.showMessageDialog(rootPane, "Please resolve captcha if exist.", "Captcha", JOptionPane.INFORMATION_MESSAGE);
        try {
            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Sign up'])[1]/following::span[1]")).click();
            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='reCAPTCHA'])[1]/preceding::div[8]")).click();
        } catch (Exception f) {
        }
        writeToCsv("https://www.tumblr.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerBlogger() throws Exception {
        driver.get("https://www.blogger.com/about/");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Publish your passions, your way'])[1]/following::a[1]")).click();
//        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Signed out'])[3]/following::div[5]")).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.id("identifierId")).clear();
        driver.findElement(By.id("identifierId")).sendKeys(acc_email_txt.getText());
        driver.findElementByCssSelector("#identifierNext").click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.name("hiddenPassword")).clear();
        driver.findElement(By.name("hiddenPassword")).sendKeys(pw.getText());
        driver.findElement(By.id("identifierId")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Learn more'])[1]/following::span[2]")).click();
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(pw.getText());
        driver.findElement(By.name("password")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Too many failed attempts'])[1]/following::span[8]")).click();
        driver.findElement(By.id("newBlog-address")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Create blog!'])[1]/following::button[1]")).click();
        writeToCsv("https://www.tumblr.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerFacebook() throws Exception {
        driver.get("https://en-gb.facebook.com/r.php?locale=en_GB");
        driver.findElement(By.id("u_0_q")).clear();
        driver.findElement(By.id("u_0_q")).sendKeys(name_txt.getText());
        driver.findElement(By.id("u_0_s")).clear();
        driver.findElement(By.id("u_0_s")).sendKeys(acc_name_txt.getText());
        driver.findElement(By.id("u_0_v")).click();
        driver.findElement(By.id("u_0_v")).clear();
        driver.findElement(By.id("u_0_v")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("u_0_y")).click();
        driver.findElement(By.id("u_0_y")).clear();
        driver.findElement(By.id("u_0_y")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("u_0_12")).click();
        driver.findElement(By.id("u_0_12")).clear();
        driver.findElement(By.id("u_0_12")).sendKeys(pw.getText());
        driver.findElement(By.id("day")).click();
        new Select(driver.findElement(By.id("day"))).selectByVisibleText("23");
        driver.findElement(By.id("day")).click();
        driver.findElement(By.id("month")).click();
        new Select(driver.findElement(By.id("month"))).selectByVisibleText("Jan");
        driver.findElement(By.id("month")).click();
        driver.findElement(By.id("year")).click();
        new Select(driver.findElement(By.id("year"))).selectByVisibleText("1995");
        driver.findElement(By.id("year")).click();
        driver.findElement(By.id("u_0_7")).click();
        driver.findElement(By.id("u_0_19")).click();
        writeToCsv("https://en-gb.facebook.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerLinkedin() throws Exception {
        driver.get("https://www.linkedin.com/start/join?");
        driver.findElement(By.id("first-name")).click();
        driver.findElement(By.id("first-name")).clear();
        driver.findElement(By.id("first-name")).sendKeys(name_txt.getText());
        driver.findElement(By.id("last-name")).clear();
        driver.findElement(By.id("last-name")).sendKeys(acc_name_txt.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Not you?'])[1]/following::div[1]")).click();
        driver.findElement(By.id("join-email")).clear();
        driver.findElement(By.id("join-email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("join-password")).click();
        driver.findElement(By.id("join-password")).clear();
        driver.findElement(By.id("join-password")).sendKeys(pw.getText());
        driver.findElement(By.id("submit-join-form-text")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='reCAPTCHA'])[1]/preceding::div[8]")).click();
        writeToCsv("https://www.linkedin.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerMoblog() throws Exception {
        driver.get("http://moblog.net/profile/create/");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.id("id_0-username")).click();
        driver.findElement(By.id("id_0-username")).clear();
        driver.findElement(By.id("id_0-username")).sendKeys(name_txt.getText());
        driver.findElement(By.id("id_0-email")).click();
        driver.findElement(By.id("id_0-email")).clear();
        driver.findElement(By.id("id_0-email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("id_0-phone")).click();
        driver.findElement(By.id("id_0-phone")).clear();
        driver.findElement(By.id("id_0-phone")).sendKeys("+94711578963");
        driver.findElement(By.id("id_0-password")).click();
        driver.findElement(By.id("id_0-password")).clear();
        driver.findElement(By.id("id_0-password")).sendKeys("root");
        driver.findElement(By.id("id_0-password_confirm")).click();
        driver.findElement(By.id("id_0-password_confirm")).clear();
        driver.findElement(By.id("id_0-password_confirm")).sendKeys("root");
        driver.findElement(By.id("id_0-tandc")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='privacy policy'])[1]/following::input[1]")).click();
        writeToCsv("http://moblog.net/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), "root");
    }

    private void registerOverBlog() throws Exception {
        driver.get("https://en.over-blog.com/signup");
        driver.findElement(By.name("email")).click();
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.name("password")).click();
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(pw.getText());
        driver.findElement(By.name("hostname")).click();
        driver.findElement(By.name("hostname")).clear();
        driver.findElement(By.name("hostname")).sendKeys(name_txt.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='.over-blog.com'])[1]/following::span[2]")).click();
        writeToCsv("https://en.over-blog.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerLiveJournal() throws Exception {
        driver.get("https://www.livejournal.com/create");
        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys(name_txt.getText() + "1");
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("password")).click();
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys(pw.getText() + "aA1");
        driver.findElement(By.id("day")).click();
        new Select(driver.findElement(By.id("day"))).selectByVisibleText("1");
        driver.findElement(By.id("day")).click();
        driver.findElement(By.id("month")).click();
        driver.findElement(By.id("month")).click();
        new Select(driver.findElement(By.id("month"))).selectByVisibleText("January");
        driver.findElement(By.id("year")).click();
        new Select(driver.findElement(By.id("year"))).selectByVisibleText("1995");
        driver.findElement(By.id("year")).click();
        driver.findElement(By.id("gender")).click();
        new Select(driver.findElement(By.id("gender"))).selectByVisibleText("Male");
        driver.findElement(By.id("gender")).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        driver.findElement(By.id("createpage_create")).click();
        speech.resolveCaptcha();
        JOptionPane.showMessageDialog(rootPane, "Please resolve captcha if exist.", "Captcha", JOptionPane.INFORMATION_MESSAGE);
        writeToCsv("https://www.livejournal.com/", name_txt.getText() + "1", acc_name_txt.getText(), acc_email_txt.getText(), pw.getText() + "aA1");
    }

    private void registerWeebly() throws Exception {
        driver.get("https://www.weebly.com/signup");
        driver.findElement(By.id("overlay-signup-form-name")).click();
        driver.findElement(By.id("overlay-signup-form-name")).clear();
        driver.findElement(By.id("overlay-signup-form-name")).sendKeys(name_txt.getText());
        driver.findElement(By.id("overlay-signup-form-email")).click();
        driver.findElement(By.id("overlay-signup-form-email")).clear();
        driver.findElement(By.id("overlay-signup-form-email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("overlay-signup-form-pass")).click();
        driver.findElement(By.id("overlay-signup-form-pass")).clear();
        driver.findElement(By.id("overlay-signup-form-pass")).sendKeys(pw.getText());
        driver.findElement(By.id("overlay-signup-form-country")).click();
        new Select(driver.findElement(By.id("overlay-signup-form-country"))).selectByVisibleText("🇺🇸 United States");
        driver.findElement(By.id("overlay-signup-form-country")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Get Started with Weebly'])[1]/following::input[4]")).click();
        writeToCsv("https://www.weebly.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerIssuu() throws Exception {
        driver.get("https://issuu.com/signup?");
        try {
            driver.findElement(By.xpath("//*[@id=\"signup-email\"]")).click();
        } catch (Exception f) {
            Thread.sleep(5000);
            driver.findElement(By.xpath("//*[@id=\"signup-email\"]")).click();
        }
        driver.findElement(By.xpath("//*[@id=\"signup-email\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"signup-email\"]")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.xpath("//*[@id=\"signup-password\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"signup-password\"]")).clear();
        driver.findElement(By.xpath("//*[@id=\"signup-password\"]")).sendKeys(pw.getText());
        driver.findElement(By.xpath("/html/body/div[3]/section[1]/div[2]/form/div[2]/div[2]/label[1]/input")).click();
        driver.findElement(By.xpath("/html/body/div[3]/section[1]/div[2]/form/div[2]/div[2]/label[2]/input")).click();
        driver.findElement(By.xpath("/html/body/div[3]/section[1]/div[2]/form/div[3]/button")).click();
        Thread.sleep(2000);
        try {
            driver.findElement(By.xpath("/html/body/div[3]/section[2]/div/form/div[4]/button")).click();
        } catch (Exception v) {
        }
        speech.resolveCaptcha();
        JOptionPane.showMessageDialog(rootPane, "Please resolve captcha if exist.", "Captcha", JOptionPane.INFORMATION_MESSAGE);
        try {
            driver.findElement(By.xpath("/html/body/div[3]/section[2]/div/form/div[4]/button")).click();
            Thread.sleep(2000);
        } catch (Exception s) {
        }
        try {
            driver.findElement(By.xpath("/html/body/div[3]/div/a")).click();
            Thread.sleep(2000);
        } catch (Exception f) {
        }
        writeToCsv("https://issuu.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerBuzzfeed() throws Exception {
        driver.get("https://www.buzzfeed.com/auth/signup");
        driver.findElement(By.id("email")).click();
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("pass")).click();
        driver.findElement(By.id("pass")).clear();
        driver.findElement(By.id("pass")).sendKeys(pw.getText());
        driver.findElement(By.id("re_enter_pass")).click();
        driver.findElement(By.id("re_enter_pass")).clear();
        driver.findElement(By.id("re_enter_pass")).sendKeys(pw.getText());
        driver.findElement(By.id("username")).click();
        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys(name_txt.getText());
        driver.findElement(By.id("signup")).click();
        writeToCsv("https://www.buzzfeed.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerReddit() throws Exception {
        driver.get("https://www.reddit.com/register/");
        driver.findElement(By.id("regEmail")).click();
        driver.findElement(By.id("regEmail")).clear();
        driver.findElement(By.id("regEmail")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::button[1]")).click();
        driver.findElement(By.id("regUsername")).click();
        driver.findElement(By.id("regUsername")).clear();
        driver.findElement(By.id("regUsername")).sendKeys(name_txt.getText());
        driver.findElement(By.id("regPassword")).click();
        driver.findElement(By.id("regPassword")).clear();
        driver.findElement(By.id("regPassword")).sendKeys(pw.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='reCAPTCHA'])[1]/preceding::div[8]")).click();
        speech.resolveCaptcha();
        JOptionPane.showMessageDialog(rootPane, "Please resolve captcha if exist.", "Captcha", JOptionPane.INFORMATION_MESSAGE);
        try {
            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::button[1]")).click();
            Thread.sleep(5000);
        } catch (Exception d) {
        }
        try {
            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='•'])[295]/following::button[2]")).click();
        } catch (Exception d) {
        }
        writeToCsv("https://www.reddit.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerVimeo() throws Exception {
        driver.get("https://vimeo.com/");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Skip to main content'])[1]/following::span[1]")).click();
        driver.findElement(By.id("signup_name")).click();
        driver.findElement(By.id("signup_name")).clear();
        driver.findElement(By.id("signup_name")).sendKeys(name_txt.getText());
        driver.findElement(By.id("signup_email")).click();
        driver.findElement(By.id("signup_email")).clear();
        driver.findElement(By.id("signup_email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("signup_password")).click();
        driver.findElement(By.id("signup_password")).clear();
        driver.findElement(By.id("signup_password")).sendKeys(pw.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Join Vimeo'])[1]/following::input[7]")).click();
        writeToCsv("https://vimeo.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerMix() throws Exception {
        driver.get("https://mix.com/!MzY1NjVm:sign-up");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Sign in'])[1]/preceding::button[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Twitter'])[2]/following::div[1]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='mohanapunchi1992@gmail.com'])[1]/following::div[5]")).click();
        driver.findElement(By.id("identifierId")).clear();
        driver.findElement(By.id("identifierId")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.name("hiddenPassword")).clear();
        driver.findElement(By.name("hiddenPassword")).sendKeys(pw.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Forgot email?'])[1]/following::span[2]")).click();
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys(pw.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)=concat('To continue, first verify it', \"'\", 's you')])[1]/following::span[7]")).click();
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Contact Support'])[1]/following::button[1]")).click();
        writeToCsv("https://mix.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void registerQuora() throws Exception {
        driver.get("https://www.quora.com/?prevent_redirect=1#");
        driver.findElement(By.id("__w2_wTXrWpjx8_continue_with_email")).click();
        driver.findElement(By.id("__w2_wTXrWpjx12_first_name")).click();
        driver.findElement(By.id("__w2_wTXrWpjx12_first_name")).clear();
        driver.findElement(By.id("__w2_wTXrWpjx12_first_name")).sendKeys(name_txt.getText());
        driver.findElement(By.id("__w2_wTXrWpjx12_last_name")).click();
        driver.findElement(By.id("__w2_wTXrWpjx12_last_name")).clear();
        driver.findElement(By.id("__w2_wTXrWpjx12_last_name")).sendKeys(acc_name_txt.getText());
        driver.findElement(By.id("__w2_wTXrWpjx12_email")).click();
        driver.findElement(By.id("__w2_wTXrWpjx12_email")).clear();
        driver.findElement(By.id("__w2_wTXrWpjx12_email")).sendKeys(acc_email_txt.getText());
        driver.findElement(By.id("__w2_wTXrWpjx12_password")).click();
        driver.findElement(By.id("__w2_wTXrWpjx12_password")).clear();
        driver.findElement(By.id("__w2_wTXrWpjx12_password")).sendKeys(pw.getText());
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='reCAPTCHA'])[1]/preceding::div[8]")).click();
        speech.resolveCaptcha();
        JOptionPane.showMessageDialog(rootPane, "Please resolve captcha if exist.", "Captcha", JOptionPane.INFORMATION_MESSAGE);
        try {
            driver.findElement(By.id("__w2_wTXrWpjx8_submit")).click();
        } catch (Exception f) {
        }
        writeToCsv("https://mix.com/", name_txt.getText(), acc_name_txt.getText(), acc_email_txt.getText(), pw.getText());
    }

    private void status(String text) {
        if (stat.length() > 5000) {
            stat = "";
        }
        stat = stat + "\n" + text;
        jTextArea1.setText(stat);
    }
}
//wordpress - done
//tublr - done
//blogger -done
//facebook -done
//linkedin -done
//moblog- done
//over-blog - done
//live journal -done
//moonfruit - done
//weebly -done
//issuu -done
//mix -done
//buzzfeed -done
//reddit -done
//vimeo -done
//quora

