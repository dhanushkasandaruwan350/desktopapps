
package util;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;


public class DriverInitializer {

    public FirefoxDriver getFirefoxDriver() {
        String proxy = "anonymox-4.3-fx.xpi";

        String drive_type = getPathForOS();
        System.setProperty("webdriver.gecko.driver", drive_type);
        System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("media.volume_scale", "0.0");
        firefoxProfile.setAcceptUntrustedCertificates(true);
        firefoxProfile.setPreference("network.http.connection-timeout", 30);
        firefoxProfile.setPreference("network.http.connection-retry-timeout", 30);
        firefoxProfile.setPreference("http.response.timeout", 30);
        firefoxProfile.setPreference("dom.max_script_run_time", 30);
//        firefoxProfile.setPreference("browser.download.dir", "/home/dhanushka/Desktop");
//        firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv");
//        firefoxProfile.setPreference("pdfjs.disabled", true);  // disable the built-in viewer
//        firefoxProfile.setPreference("browser.download.folderList", 2);
//        firefoxProfile.setPreference("browser.download.panel.shown", false);
//        try {
//            firefoxProfile.addExtension(new File(proxy));
//        } catch (Exception err) {
//
//        }

        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(firefoxProfile);
        options.setHeadless(false);
        return new FirefoxDriver(options);

    }

    public FirefoxDriver getDriverForSpeech() {
        String proxy = "anonymox-4.3-fx.xpi";

        String drive_type = getPathForOS();
        System.setProperty("webdriver.gecko.driver", drive_type);
        System.setProperty("http.agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(false);
        return new FirefoxDriver(options);

    }

    public FirefoxDriver getFirefoxDriverWithProxy(String ip, String port, String username, String pass) {
        String drive_type = getPathForOS();
        System.setProperty("webdriver.gecko.driver", drive_type);
        FirefoxOptions firefoxOptions = new FirefoxOptions();

        Proxy proxy = new Proxy();
        proxy.setProxyType(Proxy.ProxyType.MANUAL);
        String proxyStr = String.format("%s:%d", ip, Integer.parseInt(port));
        proxy.setHttpProxy(proxyStr);
        proxy.setSslProxy(proxyStr);
        firefoxOptions.setProxy(proxy);
        FirefoxDriver driver = new FirefoxDriver(firefoxOptions);
        return driver;
    }

    public ChromeDriver getChromeDriver(String ip, String port, String username, String pass) {
        Proxy proxy = new Proxy();
        proxy.setProxyType(Proxy.ProxyType.MANUAL);
        String proxyStr = String.format("%s:%d", ip, Integer.parseInt(port));
        proxy.setHttpProxy(proxyStr);
        proxy.setSslProxy(proxyStr);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.setProxy(proxy);
        ChromeDriver driver = new ChromeDriver((Capabilities) proxy);
//        driver.quit();
        return driver;
    }

    private String getPathForOS() {
        if (SystemUtils.IS_OS_LINUX) {
            System.out.println("OS -- LINUX");
            return "geckodriver";
        } else if (SystemUtils.IS_OS_WINDOWS) {
            System.out.println("OS -- WINDOWS");
            return "geckodriver.exe";
        } else {
            return "geckodriver";
        }

    }
}
