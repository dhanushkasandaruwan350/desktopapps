/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.midi.VoiceStatus;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author dhanushka
 */
public class Speech {

    FirefoxDriver driver = null;

    public void speak(String text) {
        try {
            if (driver == null) {
                driver = new DriverInitializer().getDriverForSpeech();
                driver.get("https://www.naturalreaders.com/online/");
            }
            driver.findElementByCssSelector("#inputDiv").clear();
            driver.findElementByCssSelector("#inputDiv").sendKeys(text);
            driver.findElementByCssSelector("#playBtn").click();
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Speech.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void unexpectedError() {
        try {
            if (driver == null) {
                driver = new DriverInitializer().getDriverForSpeech();
                driver.get("https://www.naturalreaders.com/online/");
            }
            driver.findElementByCssSelector("#inputDiv").clear();
            driver.findElementByCssSelector("#inputDiv").sendKeys("we have an unexpected error. \n please recheck and update script.");
            driver.findElementByCssSelector("#playBtn").click();
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Speech.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void resolveCaptcha() {
        try {
            if (driver == null) {
                driver = new DriverInitializer().getDriverForSpeech();
                driver.get("https://www.naturalreaders.com/online/");
            }
            driver.findElementByCssSelector("#inputDiv").clear();
            driver.findElementByCssSelector("#inputDiv").sendKeys("required human attension \n"
                    + "                                                                      please resolve reCAPTCHA if it exist \n"
                    + "                                               and click ok button");
            driver.findElementByCssSelector("#playBtn").click();
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Speech.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void stop() {
        driver.close();
    }
}
