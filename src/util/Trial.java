/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author dhanushka
 */
public class Trial {

    DateTimeFormatter dtf;
    LocalDateTime now;

    public int getTrialDay() {
        now = LocalDateTime.now();
        dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        String[] dt = now.format(dtf).split("/");
        System.out.println(dt[2].split(" ")[0]);
        return Integer.parseInt(dt[2].split(" ")[0]);
    }

    public String getDate() {
        now = LocalDateTime.now();
        dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
        return now.format(dtf);
    }
}
