/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import cardmarket.Dto;
import com.opencsv.CSVWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.firefox.FirefoxDriver;
import sms.Contact;
import sms.NewJFrame;

/**
 *
 * @author dhanushka
 */
public class SchoolDatasheet {

    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    CSVWriter csvWriter;
    List<UsasCommon> list = new ArrayList<>();
    String file = "";
    boolean expier = false;
    JFileChooser jfc;
    String log = "";
    FileInputStream excelFile;
    Workbook workbook;
    Sheet datatypeSheet;
    Iterator<Row> iterator;
    Row currentRow;
    Iterator<Cell> cellIterator;
    Cell currentCell;
    List<Contact> contacts;
    List<Contact> contacts2;
    DefaultTableModel tableModel = null;
    int message = 1;
    FirefoxDriver driver = null;

    public void read() {

        UsasCommon common = null;
        try {
            br = new BufferedReader(new FileReader("/home/dhanushka/Desktop/usaswimming_new.csv"));
            while ((line = br.readLine()) != null) {
//                System.out.println(line);
                String[] data = line.split(cvsSplitBy);
                if (!line.contains("ZIP CODE")) {
//                    System.out.println(data[7].split("/")[0]);
                    common = new UsasCommon();
                    common.setZipCode(data[0]);
                    common.setShow(data[1]);
                    common.setTeam(data[2]);
                    common.setName(data[3]);
                    common.setEmail(data[4]);
                    common.setPhone(data[5]);
                    common.setAddress_line_1(data[6]);
                    common.setAddress_line_2(data[7]);
                    common.setCity(data[8]);
                    common.setState(data[9]);
                    if (!checkList(common)) {
                        list.add(common);
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {

        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {

                }
            }
        }

        try (CSVWriter csvWriter = new CSVWriter(new FileWriter("schooldata.csv"))) {
            List<String[]> csvRows = new LinkedList<String[]>();
            csvRows.add(new String[]{
                "SEARCH CODE",
                "SHOW",
                "TEAM",
                "NAME",
                "EMAIL",
                "CONTACT",
                "ADDRESS LINE 1",
                "ADDRESS LINE 2",
                "CITY",
                "STATE",
                "ZIP"
            });

            int c = 1;
            for (UsasCommon data : list) {
                System.out.println("writing : " + data.getState());
                try {
                    csvRows.add(new String[]{
                        data.getZipCode(),
                        data.getShow(),
                        data.getTeam(),
                        data.getName(),
                        data.getEmail(),
                        data.getPhone(),
                        data.getAddress_line_1(),
                        data.getAddress_line_2(),
                        data.getCity(),
                        data.getState().split(" ")[0],
                        data.getState().split(" ")[1]
                    });
                } catch (Exception f) {
                    System.out.println(data.getState() + "<<<<<<<<");
                }
                c++;
            }
            csvWriter.writeAll(csvRows);
        } catch (IOException ex) {
            Logger.getLogger(SchoolDatasheet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean checkList(UsasCommon common) {
        for (UsasCommon d : list) {
            if (d.getEmail().equalsIgnoreCase(common.getEmail())
                    && d.getTeam().equalsIgnoreCase(common.getTeam())
                    && d.getName().equalsIgnoreCase(common.getName())
                    && d.getPhone().equalsIgnoreCase(common.getPhone())) {
                return true;
            }
        }
        return false;
    }

}
