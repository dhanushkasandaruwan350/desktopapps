/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashan;

import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import util.DriverInitializer;

/**
 *
 * @author dhanushka
 */
public class NewClass {

    public static void main(String[] args) throws IOException {
        FirefoxDriver driver = new DriverInitializer().getFirefoxDriver();
        FirefoxDriver driver2 = new DriverInitializer().getFirefoxDriver();
        FirefoxDriver driver3 = new DriverInitializer().getFirefoxDriver();
        String url;
        String url2;
        List<Links> urls = new ArrayList<Links>();
        Dto dto = new Dto();
        List<Dto> list = new ArrayList<>();
        Links l;
        String main;
        String sub;
        driver.get("https://tirupurguide.co.in/index.php");

        for (WebElement d : driver.findElement(By.id("menu_table2")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"))) {
            url = "https://tirupurguide.co.in/categories_alphapet.php?quickval_index=";
            try {
                url = url + d.findElement(By.tagName("a")).getText();
                main = d.findElement(By.tagName("a")).getText();
                driver2.get(url);
            } catch (Exception f) {
                continue;
            }

            for (WebElement f : driver2.findElement(By.id("alpha_search")).findElement(By.tagName("tbody")).findElements(By.tagName("tr"))) {
                url2 = "https://tirupurguide.co.in/results.php?categoryvalue=";

                for (WebElement a : f.findElements(By.tagName("a"))) {
                    url2 = "https://tirupurguide.co.in/results.php?categoryvalue=";
                    url2 = url2 + a.getText().replace(" ", "+");
                    sub = a.getText();
                    l = new Links();
                    l.setLink(url2);
                    l.setMain(main);
                    l.setSub(sub);
                    urls.add(l);
                    System.out.println(url2);
                }
            }
        }
        driver2.close();
        driver.close();

        String li;
        int page;
        String first;
        String firstOne;
        String last;
        boolean firstTime;
        boolean firstOneOk;
        boolean breaked;
        firstTime = true;
        int pages = 1;
        for (Links link : urls) {
            page = 1;
            breaked = false;
            first = "";
            last = "";
            while (true) {
                li = link.getLink() + "&entrant=" + page;
                try {
                    driver3.get(li);
                } catch (Exception d) {
                    break;
                }
                firstOne = "";
                firstOneOk = false;

                try {
                    for (WebElement tr : driver3.findElementByCssSelector("#content_table3 > tbody:nth-child(1)")
                            .findElements(By.xpath("./*"))) {
                        if (driver3.findElementByCssSelector("#content_table3 > tbody:nth-child(1)")
                                .getAttribute("innerText").contains("No Relevant Results Found.")) {
                            breaked = true;
                            System.out.println("BREAKING FIRST");
                            break;
                        }
                        for (WebElement td : tr.findElements(By.className("searchresult_contentt"))) {
                            dto = new Dto();
                            try {
                                dto.setCompany(td.findElement(By.className("search_title")).getText());
                                if (last.equalsIgnoreCase(dto.getCompany())) {
                                    breaked = true;
                                    break;
                                }
                                if (firstTime) {
                                    first = dto.getCompany();
                                    firstTime = false;
                                } else {
                                    if (first.equalsIgnoreCase(dto.getCompany())) {
                                        System.out.println("BREAKING SEC");
                                        breaked = true;
                                        break;
                                    }
                                }
                                if (!firstOneOk) {
                                    firstOne = dto.getCompany();
                                    firstOneOk = true;
                                    last = dto.getCompany();
                                } else {
                                    if (firstOne.equalsIgnoreCase(dto.getCompany()) || first.equalsIgnoreCase(dto.getCompany())) {
                                        System.out.println("BREAKING THIRD");
                                        breaked = true;
                                        break;
                                    }
                                }

                            } catch (Exception f) {
                                continue;
                            }
                            dto.setSub(link.getSub());
                            dto.setMain(link.getMain());

                            for (String v : td.getAttribute("innerText").split("\n")) {
                                if (v.contains("PH:")) {
                                    dto.setPh(v.replace("PH:", ""));
                                } else if (v.contains("Cell:")) {
                                    dto.setCell(v.replace("Cell:", ""));
                                } else if (v.contains("E-mail:")) {
                                    dto.setEmail(v.replace("E-mail:", ""));
                                } else if (v.contains("Web-site:")) {
                                    dto.setWeb(v.replace("Web-site:", ""));
                                }
                            }
                            dto.setAddress(td.getAttribute("innerHTML").split("<br>")[1]);
                            System.out.println(dto.getAddress());
                            list.add(dto);
                        }
                        if (breaked) {
                            break;
                        }
                    }
                } catch (Exception f) {
                    break;
                }
                if (breaked || driver3.findElementByCssSelector("#content_table3 > tbody:nth-child(1)")
                        .findElements(By.xpath("./*")).size() < 7) {
                    break;
                }
                page++;
            }
        }

        CSVWriter csvWriter = new CSVWriter(new FileWriter("./hashan.csv"));
        List<String[]> csvRows = new LinkedList<String[]>();
        csvRows.add(new String[]{
            "#",
            "Main Categories",
            "Sub Categories",
            "Company Name",
            "Address",
            "PH",
            "Cell",
            "Email",
            "Web Site"
        });
        int count = 1;
        for (Dto data : list) {
            csvRows.add(new String[]{
                "" + count,
                data.getMain(),
                data.getSub(),
                data.getCompany(),
                data.getAddress(),
                data.getPh().replace(",", "\n").trim(),
                data.getCell().replace(",", "\n").trim(),
                data.getEmail(),
                data.getWeb()

            });
            count++;
        }
        csvWriter.writeAll(csvRows);
        csvWriter.close();
    }

}
