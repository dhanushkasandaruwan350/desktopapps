/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package post;

import com.opencsv.CSVWriter;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import util.DriverInitializer;

/**
 *
 * @author dhanushka
 */
public class Main extends javax.swing.JFrame {

    private FirefoxDriver driver = null;
    private JavascriptExecutor executor;
    ComicBookCommon comicBookCommon = null;
    List<ComicBookCommon> comicBookCommonList = new ArrayList<>();
    List<String> postedItems = new ArrayList<>();

    private void initialise() throws InterruptedException {
        driver = new DriverInitializer().getFirefoxDriver();
        getPostedItems();
        scrape();
    }

    private void scrape() throws InterruptedException {
        String urlPost = "";
//        www.comicbookmovie.com/
        List<String> list = new ArrayList<>();
        driver.get("https://www.comicbookmovie.com/news/");
        Thread.sleep(5000);
        try {
            driver.findElement(By.id("tyche_cmp_modal")).findElement(By.className("banner_continueBtn--3KNKl")).click();
            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Show Details'])[1]/following::span[1]")).click();
        } catch (Exception e) {

        }
//        Thread.sleep(10000);
        try {
            executor.executeScript("arguments[0].click();", driver.findElementByCssSelector(".closebutton_closeButton--3abym"));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "hello. we have unexpected content to remove. press ok when done.");
        }
        try {
            driver.findElement(By.id("tyche_cmp_modal")).findElement(By.className("banner_continueBtn--3KNKl")).click();
            driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Show Details'])[1]/following::span[1]")).click();
        } catch (Exception e) {

        }
        executor = (JavascriptExecutor) driver;
        WebElement showMore;
        WebElement elementByCssSelector;
        WebElement grid;
        String url;
        boolean next = false;
        while (true) {
            showMore = driver.findElementByCssSelector("#lazyHButton").findElement(By.tagName("a"));
            executor.executeScript("arguments[0].scrollIntoView(true);", showMore);
            showMore.click();
            Thread.sleep(5000);
            for (WebElement dd : driver.findElementByCssSelector("#nLazyHeadlines").findElements(By.className("headline"))) {
                url = urlPost + "" + dd.findElement(By.tagName("a")).getAttribute("href");
                System.out.println(url);
                if (checkAvailability(url)) {
                    continue;
                }
                list.add(url);
                postedItems.add(url);
                if (list.size() > 5) {
                    next = true;
                    break;
                }
            }
            if (next) {
                break;
            }
        }
        screpeInnerPage(list);
    }

    private void screpeInnerPage(List<String> list) throws InterruptedException {
        List<ComicBookCommon> comicBookCommonArrayList = new ArrayList<>();
        for (String s : list) {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            try {
                driver.get(s);
                System.out.println("loaded");
            } catch (Exception r) {
                System.out.println("error");
            }
            String text = "";
            String trimTexst = "";
            String hearder = "";
            String s1 = "";
            Thread.sleep(5000);
            WebElement articles = driver.findElementByCssSelector("#middle").findElement(By.id("left")).findElement(By.className("article"));
            executor = (JavascriptExecutor) driver;
            try {
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", articles.findElement(By.id("ListButtons1")));
                ((JavascriptExecutor) driver).executeScript("arguments[0].click();", articles.findElement(By.className("button")));
                Thread.sleep(2000);
            } catch (Exception e) {

            }
            for (WebElement element : articles.findElements(By.xpath("./*"))) {
                if (element.getTagName().equalsIgnoreCase("h1")) {

                    hearder = element.findElement(By.tagName("span")).getAttribute("innerText");
                } else if (element.getAttribute("class").contains("body")) {
                    text = element.getAttribute("innerText");
                    trimTexst = text.replaceAll("\\s{2,}", " ").trim();
                    s1 = trimTexst.replace("Report Ad", " ");
                   

                    comicBookCommon = new ComicBookCommon();
                    comicBookCommon.setHeader(hearder);
                    comicBookCommon.setTitle(s1);
                    comicBookCommonArrayList.add(comicBookCommon);
                    break;
                }
            }
        }
        convertText(comicBookCommonArrayList);
    }

    private void convertText(List<ComicBookCommon> comicBookCommonArrayList) throws InterruptedException {
        List<ComicBookCommon> bookCommonsList = new ArrayList<>();
        WebElement testArea;
        WebElement quillBtn;
        WebElement testConvert;
        String body = "";
        int c = 0;
        int restry = 0;
        driver.get("https://quillbot.com/");
        Thread.sleep(5000);
        WebElement loginBtn = driver.findElementByCssSelector("div.login-offer-text:nth-child(1)");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", loginBtn);
        Thread.sleep(2000);
        WebElement emailInput = driver.findElementByCssSelector("#username");
        WebElement passInput = driver.findElementByCssSelector("#password");
        emailInput.sendKeys(email.getText());
        passInput.sendKeys(pw.getText());
        WebElement login = driver.findElementByCssSelector("div.ml-login-btn:nth-child(4)");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", login);
        JOptionPane.showMessageDialog(rootPane, "Please Resolve the sequrity question manually.", "Need human interaction", JOptionPane.WARNING_MESSAGE);
        Thread.sleep(5000);
        WebElement newText;
        String text;
        String text2;
        String res;
        int start;
        int end;
        int conv = 0;
        int val = 200;
        for (ComicBookCommon commonText : comicBookCommonArrayList) {
            res = "";
            text = commonText.getTitle();

            if (text.length() > 5000) {
                val = 5000;
            } else {
                val = text.length();
            }
            for (int i = 0; i < text.length(); i += val) {
                text2 = "";
                try {
                    text2 = text.substring(i, i + val);
                } catch (Exception f) {
                    text2 = text.substring(i, text.length());
                }

                testArea = driver.findElementByCssSelector("#inputText");
                testArea.sendKeys(text2);

                quillBtn = driver.findElementByCssSelector("#paraphraseBtn");
                quillBtn.click();
                Thread.sleep(15000);
                try {
                    testConvert = driver.findElementByCssSelector("#articleTextArea");
                } catch (Exception f) {
                    testConvert = driver.findElementByCssSelector("#editableOutput");
                }
                body = testConvert.getAttribute("innerText");
                res = res + body;
                

                driver.navigate().refresh();
            }
            System.out.println(res);
            comicBookCommon = new ComicBookCommon();
            comicBookCommon.setHeader(commonText.getHeader());
            comicBookCommon.setTitle(res);
            bookCommonsList.add(comicBookCommon);
            c++;
            con.setText(conv + " Posts Are Converted!");
            conv++;
        }
        wordpress(bookCommonsList);
    }

    private void wordpress(List<ComicBookCommon> bookCommonsList) throws InterruptedException {
        driver.get("https://www.epicheroes.com/wp-admin/");
        Thread.sleep(5000);
        WebElement body;
        WebDriver iframe;
        WebElement titlePost;
        WebElement newPost;
        WebElement userLogin = driver.findElementByCssSelector("#user_login");
        userLogin.sendKeys(email.getText());
        WebElement userPass = driver.findElementByCssSelector("#user_pass");
        userPass.sendKeys(pw.getText());
        JOptionPane.showMessageDialog(rootPane, "Please resolve captcha and click ok when you done", "security check", JOptionPane.WARNING_MESSAGE);
        WebElement loginBtn = driver.findElementByCssSelector("#wp-submit");
        loginBtn.click();
        Thread.sleep(15000);
        WebElement postPage = driver.findElementByCssSelector("a.menu-icon-post > div:nth-child(3)");
        postPage.click();
        Thread.sleep(5000);
        for (ComicBookCommon bookCommon : bookCommonsList) {
            newPost = driver.findElementByCssSelector(".page-title-action");
            newPost.click();
            Thread.sleep(10000);
            titlePost = driver.findElementByCssSelector("#title");
            titlePost.sendKeys(bookCommon.getHeader());
            Thread.sleep(2000);
            driver.switchTo().frame(1);
            body = driver.findElementByCssSelector("#tinymce > p:nth-child(1)");
            driver.executeScript("arguments[0].textContent = arguments[1];", body, bookCommon.getTitle());
            driver.switchTo().defaultContent();
            WebElement savePost = driver.findElementByCssSelector("#save-post");
            savePost.click();
            Thread.sleep(5000);
        }
        JOptionPane.showMessageDialog(rootPane, "Done!", "Posted", JOptionPane.INFORMATION_MESSAGE);
        jLabel9.setText("START");
        writePostedItems();
    }

    public Main() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        pw = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        con = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(254, 254, 254));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(254, 254, 254));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/post/blogger_128_black.png"))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        jLabel5.setText("WELCOME BACK,");

        jLabel6.setFont(new java.awt.Font("Sarai", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(162, 150, 150));
        jLabel6.setText("Enter account details to continue");

        jLabel7.setFont(new java.awt.Font("Sarai", 1, 18)); // NOI18N
        jLabel7.setText("Password");

        jLabel8.setFont(new java.awt.Font("Sarai", 1, 18)); // NOI18N
        jLabel8.setText("Email");

        email.setText("zohe@growthhakka.com");

        pw.setText("Quillbot");

        jLabel9.setBackground(new java.awt.Color(138, 19, 216));
        jLabel9.setFont(new java.awt.Font("SansSerif", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(254, 254, 254));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("START");
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel9.setOpaque(true);
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/watching/Knob Blue.png"))); // NOI18N
        jButton2.setContentAreaFilled(false);
        jButton2.setDefaultCapable(false);
        jButton2.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/watching/Knob Remove Red.png"))); // NOI18N
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/watching/Knob Blue.png"))); // NOI18N
        jButton1.setContentAreaFilled(false);
        jButton1.setDefaultCapable(false);
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/com/oriant/watching/Knob Cancel.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        con.setFont(new java.awt.Font("Sarai", 3, 15)); // NOI18N
        con.setForeground(new java.awt.Color(124, 107, 145));
        con.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel7)
                                    .addComponent(email)
                                    .addComponent(pw)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(30, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(con, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(64, 64, 64)
                .addComponent(jLabel8)
                .addGap(9, 9, 9)
                .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pw, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(con, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {email, pw});

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 0, 440, 540));

        jPanel2.setBackground(new java.awt.Color(138, 19, 216));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/post/gnome_blog.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Pagul", 1, 15)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(254, 254, 254));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("WELCOME TO");

        jLabel3.setFont(new java.awt.Font("Sarai", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(254, 254, 254));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("jPost");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(121, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(116, 116, 116))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addContainerGap(213, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 380, 540));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        jLabel9.setText("Please Wait");
        new Thread(() -> {
            try {
                initialise();
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        setState(ICONIFIED);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel con;
    private javax.swing.JTextField email;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField pw;
    // End of variables declaration//GEN-END:variables

    private void getPostedItems() {
        String fileToParse = "postedItems.csv";
        BufferedReader fileReader = null;
        final String DELIMITER = ",";
        try {
            String line = "";
            fileReader = new BufferedReader(new FileReader(fileToParse));

            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(DELIMITER);
                try {
                    if (tokens[0].replace("\"", "").equalsIgnoreCase("Link")) {
                        continue;
                    }
                    postedItems.add(tokens[0].replace("\"", ""));
                } catch (ArrayIndexOutOfBoundsException e) {
                }
            }
        } catch (Exception e) {
        } finally {
            try {
                assert fileReader != null;
                fileReader.close();
            } catch (IOException | NullPointerException e) {
            }
        }

    }

    private boolean checkAvailability(String url) {
        for (String s : postedItems) {
            if (s.equalsIgnoreCase(url)) {
                return true;
            }
        }
        return false;
    }

    private void writePostedItems() {
        try {
            CSVWriter csvWriter = new CSVWriter(new FileWriter("postedItems.csv"));
            List<String[]> csvRows = new LinkedList<String[]>();
            csvRows.add(new String[]{
                "Link"
            });
            for (String s : postedItems) {
                csvRows.add(new String[]{s});
            }
            csvWriter.writeAll(csvRows);
            csvWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
