/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imagedownloader;

/**
 *
 * @author dhanushka
 */
public class Dto {
    private String id;
    private String comp;
    private String kode;
    private String partscrama;
    private String partdimen;
    private String matches;

    
    public String getComp() {
        return comp;
    }

    public void setComp(String comp) {
        this.comp = comp;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPartscrama() {
        return partscrama;
    }

    public void setPartscrama(String partscrama) {
        this.partscrama = partscrama;
    }

    public String getPartdimen() {
        return partdimen;
    }

    public void setPartdimen(String partdimen) {
        this.partdimen = partdimen;
    }

    public String getMatches() {
        return matches;
    }

    public void setMatches(String matches) {
        this.matches = matches;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
}
