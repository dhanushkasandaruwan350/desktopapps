
import com.opencsv.CSVWriter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import knowem.NewJFrame;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import util.DriverInitializer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dhanushka
 */
public class soleger extends javax.swing.JFrame {

    BufferedReader br = null;
    String line = "";
    String cvsSplitBy = ",";
    CSVWriter csvWriter;
    private JavascriptExecutor executor;
    CSVWriter account;

    public soleger() {
        initComponents();
        try {
            startScrape();
        } catch (InterruptedException ex) {
            Logger.getLogger(soleger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> getLinks() {
        List<String> list = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader("/home/dhanushka/Downloads/imageURL-Sheet1.csv"));
            while ((line = br.readLine()) != null) {
                String[] country = line.split(cvsSplitBy);
                if (!line.contains("URL")) {
                    list.add(line.replace("\"", ""));
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {

        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {

                }
            }
        }
        return list;
    }

    private void startScrape() throws InterruptedException {
        FirefoxDriver driver = new DriverInitializer().getFirefoxDriver();
        List<String> links = getLinks();
        SolegorData d;
        Apartmant apartmant;
        int id = 1;
        int id2 = 1;
        int apart;
        int park;
        int bure;
        int znd;
        int team;
        for (String link : links) {
            driver.get(link);
            executor = (JavascriptExecutor) driver;
            Thread.sleep(3000);
            apart = 0;
            park = 0;
            bure = 0;
            znd = 1;
            team = 1;
            d = new SolegorData();
            d.setId(id);
            try {
                d.setName(driver.findElementByCssSelector(".title-content > div:nth-child(1)").getAttribute("innerText"));
            } catch (Exception s) {
                JOptionPane.showMessageDialog(rootPane, "chaptcha");
            }
            try {
                d.setAddress(driver.findElementByCssSelector("div.subtitle:nth-child(4)").getAttribute("innerText"));
            } catch (Exception v) {
                try {
                    d.setAddress(driver.findElementByCssSelector("div.subtitle:nth-child(3)").getAttribute("innerText"));
                } catch (Exception x) {
                    try {
                        d.setAddress(driver.findElementByCssSelector("div.subtitle:nth - child(5)").getAttribute("innerText"));
                    } catch (Exception n) {
                        try {
                            d.setAddress(driver.findElementByCssSelector(".title-content").findElement(By.className("subtitle")).getAttribute("innerText"));
                        } catch (Exception b) {
                        }
                    }
                }
            }
            try {
                d.setWeb(driver.findElementByCssSelector("a.b-link:nth-child(5)").getAttribute("href"));
            } catch (Exception l) {
                try {
                    d.setWeb(driver.findElementByCssSelector("a.b-link:nth-child(4)").getAttribute("href"));
                } catch (Exception b) {
                }
            }
            driver.findElementByCssSelector(".phone-tab").click();

            try {
                d.setNumber(driver.findElementByCssSelector("div.u-lefted:nth-child(1)").getAttribute("innerText"));
            } catch (Exception f) {
                d.setNumber(driver.findElementByCssSelector("div.panel:nth-child(2) > div:nth-child(1)").getAttribute("innerText"));
            }
            executor.executeScript("arguments[0].scrollIntoView(true);", driver.findElementByCssSelector("div.tab-container:nth-child(2) > div:nth-child(1)"));
            try {
                d.setBienenvente(driver.findElementByCssSelector("div.selling-tab > div:nth-child(1)").getAttribute("innerText"));
            } catch (Exception v) {
                Thread.sleep(1000);
                try {
                    d.setBienenvente(driver.findElementByCssSelector("div.tab-container:nth-child(2) > div:nth-child(1)")
                            .findElement(By.className("quantity")).getAttribute("innerText"));
                } catch (Exception n) {
                }
            }
            try {
                d.setBienenlocationtot(driver.findElementByCssSelector("div.renting-tab > div:nth-child(1)").getAttribute("innerText"));
            } catch (Exception f) {
                d.setBienenlocationtot("");
            }

            try {
                executor.executeScript("arguments[0].scrollIntoView(true);", driver.findElementByCssSelector("span.tab:nth-child(2)"));
                executor.executeScript("arguments[0].click();", driver.findElementByCssSelector("span.tab:nth-child(2)"));
                executor.executeScript("arguments[0].scrollIntoView(true);", driver.findElementByCssSelector(".b-ghost"));
                executor.executeScript("arguments[0].click();", driver.findElementByCssSelector(".b-ghost"));
            } catch (Exception f) {
            }

            try {
                for (WebElement s : driver.findElementByCssSelector(".biens").findElements(By.xpath("./*"))) {
                    apartmant = new Apartmant();
                    apartmant.setId(id2);
                    apartmant.setAgency(id);
                    try {
                        apartmant.setType(s.findElement(By.className("c-pa-info")).findElement(By.className("c-pa-link")).getAttribute("innerText"));
                    } catch (Exception x) {
                        apartmant.setType(s.findElement(By.className("ag-bien-vendu")).findElement(By.className("title")).getAttribute("innerText"));
                    }
                    try {
                        apartmant.setLocal(s.findElement(By.className("c-pa-info")).findElement(By.className("c-pa-where")).getAttribute("innerText"));
                    } catch (Exception m) {
                        apartmant.setLocal(s.findElement(By.className("ag-bien-vendu")).findElement(By.className("city")).getAttribute("innerText"));
                    }
                    try {
                        apartmant.setPrix(s.findElement(By.className("c-pa-info")).findElement(By.className("c-pa-price")).getAttribute("innerText"));
                    } catch (Exception l) {
                        apartmant.setPrix(s.findElement(By.className("ag-bien-vendu")).findElement(By.className("sold-details")).getAttribute("innerText").split(" ")[1].split("en")[0]);
                    }
                    if (apartmant.getType().contains("Appartement")) {
                        apart++;
                    } else if (apartmant.getType().contains("Bureaux")) {
                        bure++;
                    } else if (apartmant.getType().contains("Parking")) {
                        park++;
                    }
                    try {
                        for (WebElement i : s.findElement(By.className("c-pa-info")).findElement(By.className("c-pa-tags")).findElements(By.xpath("./*"))) {
                            if (i.getAttribute("innerText").contains("p")) {
                                apartmant.setPices(i.getAttribute("innerText"));
                            } else if (i.getAttribute("innerText").contains("ch")) {
                                apartmant.setChamber(i.getAttribute("innerText"));
                            } else if (i.getAttribute("innerText").contains("m²")) {
                                apartmant.setSerface(i.getAttribute("innerText"));
                            }
                        }
                    } catch (Exception b) {
                        for (WebElement i : s.findElement(By.className("ag-bien-vendu")).findElement(By.className("tags")).findElements(By.xpath("./*"))) {
                            if (i.getAttribute("innerText").contains("p")) {
                                apartmant.setPices(i.getAttribute("innerText"));
                            } else if (i.getAttribute("innerText").contains("ch")) {
                                apartmant.setChamber(i.getAttribute("innerText"));
                            } else if (i.getAttribute("innerText").contains("m²")) {
                                apartmant.setSerface(i.getAttribute("innerText"));
                            }
                        }
                    }
                    id2++;
                    writeApartFile(apartmant);
                }
            } catch (Exception v) {
            }
            d.setAppatrment(apart + "");
            d.setParking(park + "");
            d.setBureaux(bure + "");

            try {
                for (WebElement s : driver.findElementByCssSelector("div.ag-white-box:nth-child(2) > div:nth-child(2)").findElements(By.tagName("span"))) {
                    if (znd == 1) {
                        d.setZoned(s.getAttribute("innerText"));
                    } else if (znd == 2) {
                        d.setZoned2(s.getAttribute("innerText"));
                    } else if (znd == 3) {
                        d.setZoned3(s.getAttribute("innerText"));
                    } else if (znd == 4) {
                        d.setZoned4(s.getAttribute("innerText"));
                    } else if (znd == 5) {
                        d.setZoned5(s.getAttribute("innerText"));
                    } else if (znd == 6) {
                        d.setZoned6(s.getAttribute("innerText"));
                    }
                    znd++;
                }
            } catch (Exception f) {
            }
            try {
                for (WebElement s : driver.findElementByCssSelector(".ag-team").findElements(By.xpath("./*"))) {
                    if (team == 1) {
                        d.setTeam(s.getAttribute("innerText"));
                    } else if (team == 2) {
                        d.setTeam2(s.getAttribute("innerText"));
                    } else if (team == 3) {
                        d.setTeam3(s.getAttribute("innerText"));
                    } else if (team == 4) {
                        d.setTeam4(s.getAttribute("innerText"));
                    } else if (team == 5) {
                        d.setTeam5(s.getAttribute("innerText"));
                    } else if (team == 6) {
                        d.setTeam6(s.getAttribute("innerText"));
                    } else if (team == 7) {
                        d.setTeam7(s.getAttribute("innerText"));
                    } else if (team == 8) {
                        d.setTeam8(s.getAttribute("innerText"));
                    }
                    team++;
                }
            } catch (Exception v) {
            }

            writeAgency(d);
            System.out.println(link);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(soleger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(soleger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(soleger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(soleger.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new soleger().setVisible(true);
            }
        });
    }

    private void writeApartFile(Apartmant apartmant) {
        try {
            account = new CSVWriter(new FileWriter("appartements.csv", true));
            List<String[]> csvRows = new LinkedList<String[]>();
            csvRows.add(new String[]{apartmant.getId() + "", apartmant.getAgency() + "", apartmant.getType(), apartmant.getLocal(),
                apartmant.getPices(), apartmant.getChamber(), apartmant.getSerface(), apartmant.getPrix()});
            account.writeAll(csvRows);
            account.close();
        } catch (IOException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeAgency(SolegorData d) {
        try {
            account = new CSVWriter(new FileWriter("agences.csv", true));
            List<String[]> csvRows = new LinkedList<String[]>();
            csvRows.add(new String[]{d.getId() + "", d.getName(), d.getAddress(), d.getWeb(), d.getNumber(), d.getBienenvente(),
                d.getBienenlocationtot(), d.getAppatrment(), d.getParking(), d.getBureaux(), d.getZoned(),
                d.getZoned2(), d.getZoned3(), d.getZoned4(), d.getZoned5(), d.getZoned6(), d.getTeam(),
                d.getTeam2(), d.getTeam3(), d.getTeam4(), d.getTeam5(), d.getTeam6(), d.getTeam7(), d.getTeam8()});
            account.writeAll(csvRows);
            account.close();
        } catch (IOException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
