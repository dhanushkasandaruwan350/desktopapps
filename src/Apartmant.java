/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dhanushka
 */
public class Apartmant {

    private int id;
    private int agency;
    private String type;
    private String local;
    private String pices;
    private String chamber;
    private String serface;
    private String prix;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAgency() {
        return agency;
    }

    public void setAgency(int agency) {
        this.agency = agency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local.split(" ")[0];
    }

    public String getPices() {
        return pices;
    }

    public void setPices(String pices) {
        this.pices = pices;
    }

    public String getChamber() {
        return chamber;
    }

    public void setChamber(String chamber) {
        this.chamber = chamber;
    }

    public String getSerface() {
        return serface;
    }

    public void setSerface(String serface) {
        this.serface = serface;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

}
